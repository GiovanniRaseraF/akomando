import { expect } from 'chai';
import { getFile, createAkomando } from '../imports/utils'

describe('#AkomantosoDocumentHandler Document document_it.xml (Italy)', function() {
   let myAkomando;
   before((done) => {
      getFile('document_it.xml', (content) => {
         myAkomando = createAkomando({
            aknString: content,
         });
         done();
      })
   });

   it('After having parsed the XML string the Akn Document Element must exist', function() {
      const result = myAkomando.getAkomaNtoso().documentElement.nodeType;
      expect(result).to.equal(1);
   });
   it('After having parsed the XML string the Akn Document Element must have 1 children', function() {
      const result = myAkomando.getAkomaNtoso().documentElement.childNodes.length;
      expect(result).to.equal(1);
   });
   it('After having parsed the XML string the AKNString should be long 138064 characters', function() {
      const result = myAkomando.getAkomaNtoso({
         serialize: 'AKNString',
      }).length;
      expect(result).to.equal(138064);
   });
   it('After having parsed the XML string the HTMLString should be long 175038 characters', function() {
      const result = myAkomando.getAkomaNtoso({
         serialize: 'HTMLString',
      });
      expect(result.length).to.equal(175038);
   });
   it('After having parsed the XML string the JSONString should be long 127551 characters', function() {
      const result = myAkomando.getAkomaNtoso({
         serialize: 'JSONString',
      });
      expect(result.length).to.equal(127551);
   });
   it('After having parsed the XML string the HTML Document Element must exist', function() {
      const result = myAkomando.getAkomaNtoso({
         serialize: 'HTMLDom',
      }).documentElement.nodeType;
      expect(result).to.equal(1);
   });
   it('After having parsed the XML string the first element of the body must have class name equal to akn-akomaNtoso ', function() {
      const result = myAkomando.getAkomaNtoso({
         serialize: 'HTMLDom',
      }).getElementsByTagName('body')[0].firstChild.getAttribute('class');
      expect(result).to.equal('akn-akomaNtoso');
   });
   it('After having parsed the XML string the HTML Document Element must have 2 children', function() {
      const result = myAkomando.getAkomaNtoso({
         serialize: 'HTMLDom',
      }).documentElement.childNodes.length;
      expect(result).to.equal(2);
   });
   it('The meta in AKNDom must be unique and its name must be meta "meta"', function() {
      const meta = myAkomando.getMeta();
      const result = meta.length;
      expect(result).to.equal(1);
      expect(meta[0].nodeName).to.equal('meta');
   });
   it('The meta in AKN string format must be long 55215 character', function() {
      const meta = myAkomando.getMeta({
         serialize: 'AKNString',
      });
      const result = meta.pop().length;
      expect(result).to.equal(55215);
   });
   it('There must be three FRBRthis elements when retreiving meta in AKN format', function() {
      const meta = myAkomando.getMeta({
         serialize: 'AKNDom',
         metaRoot: 'FRBRthis',
      });
      const result = meta.length;
      expect(result).to.equal(3);
   });
   it('There must be three FRBRthis elements when retreiving meta in JSON format', function() {
      const meta = myAkomando.getMeta({
         serialize: 'JSON',
         metaRoot: 'FRBRthis',
      });
      const result = meta.length;
      expect(result).to.equal(3);
   });
   it('The meta in JSON string format must be long 56614 character', function() {
      const meta = myAkomando.getMeta({
         serialize: 'JSONString',
      });
      const result = meta.pop().length;
      expect(result).to.equal(56614);
   });
   it('There must be three FRBRthis elements when retreiving meta in HTML format', function() {
      const meta = myAkomando.getMeta({
         serialize: 'HTMLDom',
         metaRoot: 'FRBRthis',
      });
      const result = meta.length;
      expect(result).to.equal(3);
   });
   it('The meta in HTML string format must be long 72530 character', function() {
      const meta = myAkomando.getMeta({
         serialize: 'HTMLString',
      });
      const result = meta.pop().length;
      expect(result).to.equal(72530);
   });
   it('There must be one FRBRcountry element when retreiving meta in AKNDom', function() {
      const meta = myAkomando.getMeta({
         serialize: 'AKNDom',
         metaRoot: 'FRBRcountry',
      });
      const result = meta.length;
      expect(result).to.equal(1);
   });
   it('There must be three akn:FRBRthis elements when retreiving meta in AKNDom', function() {
      const meta = myAkomando.getMeta({
         serialize: 'AKNDom',
         metaRoot: 'akn:FRBRthis',
      });
      const result = meta.length;
      expect(result).to.equal(3);
   });
   it('The document type must be act', function() {
      const docType = myAkomando.getDocType();
      expect(docType).to.equal('act');
   });

   it('The document must contain 162 hierarchies. The eId of the first one must be art_1 and the eId of the last one must be art_10__para_1', function(){
      const hiers = myAkomando.getHierIdentifiers();
      expect(hiers.length).to.equal(162);
      expect(hiers[0].eId).to.equal('art_1');
      expect(hiers[hiers.length-1].eId).to.equal('art_10__para_1');
   });

   it('The document must contain 162 hierarchies when ordered descending. The eId of the first one must be art_10__para_1 and the eId of the last one must be art_10', function(){
      const hiers = myAkomando.getHierIdentifiers({
         order:'descending',
      });
      expect(hiers.length).to.equal(162);
      expect(hiers[0].eId).to.equal('art_10__para_1');
      expect(hiers[hiers.length-1].eId).to.equal('art_1');
   });

   it('The document must contain 162 hierarchies when sorted by name. The eId of the first one must be art_1 and the eId of the last one must be art_3__para_7__mod_27__qstr_2', function(){
      const hiers = myAkomando.getHierIdentifiers({
         sortBy:'name',
      });
      expect(hiers.length).to.equal(162);
      expect(hiers[0].eId).to.equal('art_1');
      expect(hiers[hiers.length-1].eId).to.equal('art_3__para_7__mod_27__qstr_2');
   });

   it('The document must contain 162 hierarchies when sorted by eId. The eId of the first one must be art_1 and the eId of the last one must be art_9__para_6', function(){
      const hiers = myAkomando.getHierIdentifiers({
         sortBy:'eId',
      });
      expect(hiers.length).to.equal(162);
      expect(hiers[0].eId).to.equal('art_1');
      expect(hiers[hiers.length-1].eId).to.equal('art_9__para_6');
   });

   it('The document must contain 162 hierarchies when sorted by wId. The eId of the first one must be art_1 and the eId of the last one must be art_3__para_6__mod_26__qstr_1__para_1-bis', function(){
      const hiers = myAkomando.getHierIdentifiers({
         sortBy:'wId',
      });
      expect(hiers.length).to.equal(162);
      expect(hiers[0].eId).to.equal('art_1');
      expect(hiers[hiers.length-1].eId).to.equal('art_3__para_6__mod_26__qstr_1__para_1-bis');
   });

   it('The document must contain 162 hierarchies when sorted by GUID. The eId of the first one must be art_1 and the eId of the last one must be art_10__para_1', function(){
      const hiers = myAkomando.getHierIdentifiers({
         sortBy:'GUID',
      });
      expect(hiers.length).to.equal(162);
      expect(hiers[0].eId).to.equal('art_1');
      expect(hiers[hiers.length-1].eId).to.equal('art_10__para_1');
   });

   it('The document must contain 162 hierarchies when sorted by xpath. The eId of the first one must be art_10 and the eId of the last one must be art_9__para_6', function(){
      const hiers = myAkomando.getHierIdentifiers({
         sortBy:'xpath',
      });
      expect(hiers.length).to.equal(162);
      expect(hiers[0].eId).to.equal('art_10');
      expect(hiers[hiers.length-1].eId).to.equal('art_9__para_6');
   });

   it('The document must contain 162 hierarchies when sorted by level. The eId of the first one must be art_1 and the eId of the last one must be art_3__para_1__list_1__point_b__list_1__point_2__mod_17__qstr_1__para_1', function(){
      const hiers = myAkomando.getHierIdentifiers({
         sortBy:'level',
      });
      expect(hiers.length).to.equal(162);
      expect(hiers[0].eId).to.equal('art_1');
      expect(hiers[hiers.length-1].eId).to.equal('art_3__para_1__list_1__point_b__list_1__point_2__mod_17__qstr_1__para_1');
   });

   it('The document must contain 10 articles. The eId of the seventh one must be art_7 and the eId of the last one must be art_10', function(){
      const hiers = myAkomando.getHierIdentifiers({
         filterByName: 'article',
      });
      expect(hiers.length).to.equal(10);
      expect(hiers[6].eId).to.equal('art_7');
      expect(hiers[hiers.length-1].eId).to.equal('art_10');
   });

   it('The document must contain 58 partitions having content. The eId of the first one must be art_1__para_1 and the eId of the seventeenth one must be art_2__para_5__list_1__point_a', function(){
      const hiers = myAkomando.getHierIdentifiers({
         filterByContent: true,
      });
      expect(hiers.length).to.equal(58);
      expect(hiers[0].eId).to.equal('art_1__para_1');
      expect(hiers[57].eId).to.equal('art_10__para_1');
   });

   it('The raw text of the document must contain 25020 characters without line breaks', function(){
      const text = myAkomando.getAkomaNtoso({ serialize: 'TEXT', newLines: false });
      expect(text.length).to.equal(25020);
   });

   it('Substring from 99 to 110 must be equal to "articolo 2,", substring from 14995 to 15020 must be equal to "le seguenti: \"Si fa luogo" and substring from 20000 to 20030 must be equal to "ente a ricevere il rapporto e "', function(){
      const text = myAkomando.getAkomaNtoso({ serialize: 'TEXT', newLines: false });
      expect(text.substring(99, 110)).to.equal('articolo 2,');
      expect(text.substring(14995, 15020)).to.equal('le seguenti: "Si fa luogo');
      expect(text.substring(20000, 20030)).to.equal('ente a ricevere il rapporto e ');
   });

   it('The raw text of the document must contain 36174 characters with line breaks', function(){
      const text = myAkomando.getAkomaNtoso({ serialize: 'TEXT', newLines: true });
      expect(text.length).to.equal(36174);
   });

   it('The first line must be equal to "DECRETO LEGISLATIVO15 gennaio 2016, n. 8Disposizioni in materia di depenalizzazione, a", the last line must be equal to "                    finanzeVisto, il Guardasigilli: Orlando" and the line "160" must be equal to "                                            "è soggetto alla sanzione amministrativa pecuniaria"', function(){
      const text = myAkomando.getAkomaNtoso({ serialize: 'TEXT', newLines: true });
      const lines = text.split('\n');
      expect(lines[0]).to.equal('DECRETO LEGISLATIVO15 gennaio 2016, n. 8Disposizioni in materia di depenalizzazione, a');
      expect(lines[160]).to.equal('                                            "è soggetto alla sanzione amministrativa pecuniaria');
      expect(lines[lines.length-1]).to.equal('                    finanzeVisto, il Guardasigilli: Orlando');
   });

   it('The total number of elements must be 1697 and element in position 17 must have name "akomaNtoso" and its count must be 1', function(){
      const elementsCount = myAkomando.countDocElements();
      expect(elementsCount.total).to.equal(1697);
      expect(elementsCount.elements[16].name).to.equal('akomaNtoso');
      expect(elementsCount.elements[16].count).to.equal(1);
   });

   it('The total number of elements ref must be 97 and element in position 0 must have name "ref" and its count must be 97', function(){
      const elementsCount = myAkomando.countDocElements({
         filterByName: 'ref',
      });
      expect(elementsCount.total).to.equal(97);
      expect(elementsCount.elements[0].name).to.equal('ref');
      expect(elementsCount.elements[0].count).to.equal(97);
   });

    it('The total number of elements mod must be 27 and element in position 0 must have name "mod" and its count must be 27', function(){
        const elementsCount = myAkomando.countDocElements({
            filterByName: 'mod',
        });
        expect(elementsCount.total).to.equal(27);
        expect(elementsCount.elements[0].name).to.equal('mod');
        expect(elementsCount.elements[0].count).to.equal(27);
    });

    it('Tests with getIdReferences: should get 171 id references, 120 existing, 51 pending', function(){
        const result_all = myAkomando.getIdReferences();
        expect(result_all.length).to.equal(171);

        let result_existing = myAkomando.getIdReferences({ filterBy: 'existing'});
        expect(result_existing.length).to.equal(120);
        expect(result_existing[0].refersTo).to.equal('supremaCorteDiCassazione');
        expect(result_existing[0].referenceLinkedInAttribute).to.equal('source');

        let result_pending = myAkomando.getIdReferences({ filterBy: 'pending' });
        expect(result_pending.length).to.equal(51);
        expect(result_pending[0].refersTo).to.equal('tesauroCassazione');
     });

    it('Tests with the XPath selection', function() {
        let selection = myAkomando.getElementsFromXPath('/akomaNtoso/act');
        expect(selection.length).to.equal(1);
        selection = myAkomando.getElementsFromXPath('/akomaNtoso/act/body/article');
        expect(selection.length).to.equal(10);
        selection = myAkomando.getElementsFromXPath('/akomaNtoso/act/meta/classification/keyword[2]');
        expect(selection.length).to.equal(1);
    });

    it('The document\'s URIs must be: Work: "/akn/it/act/decretolegislativo/stato/2016-01-15/decreto legislativo", Expression: "/akn/it/act/decretolegislativo/stato/2016-01-15/decreto legislativo/ita@" and Manifestation: "/akn/it/act/decretolegislativo/stato/2016-01-15/decreto legislativo/ita@.xml"', function() {
        const FRBR = myAkomando.getFRBR();
        expect(FRBR.work.uri).to.equal('/akn/it/act/decretolegislativo/stato/2016-01-15/decreto legislativo');
        expect(FRBR.expression.uri).to.equal('/akn/it/act/decretolegislativo/stato/2016-01-15/decreto legislativo/ita@');
        expect(FRBR.manifestation.uri).to.equal('/akn/it/act/decretolegislativo/stato/2016-01-15/decreto legislativo/ita@.xml');

        expect(FRBR.work.this).to.equal('/akn/it/act/decretolegislativo/stato/2016-01-15/decreto legislativo/!main');
        expect(FRBR.expression.this).to.equal('/akn/it/act/decretolegislativo/stato/2016-01-15/decreto legislativo/ita@/!main');
        expect(FRBR.manifestation.this).to.equal('/akn/it/act/decretolegislativo/stato/2016-01-15/decreto legislativo/ita@/main.xml/!main');
    });

    it('Test without \'about\' parameter. The expression author is stato and publication date is 2016-01-15', function(){
        const publicationInfo = myAkomando.getPublicationInfo();
        expect(publicationInfo.author.name).to.equal('stato');
        expect(publicationInfo.author.ref).to.equal('/ontology/organizations/it/Stato');
        expect(publicationInfo.date).to.equal('2016-01-15');
        /*expect(publicationInfo.publication.date).to.equal('2016-01-22');
        expect(publicationInfo.publication.name).to.equal('GU');
        expect(publicationInfo.publication.showAs).to.equal('Gazzetta Ufficiale');
        expect(publicationInfo.publication.number).to.equal('017');*/
    });

    it('The work author is stato and publication date is 2016-01-15', function(){
        const publicationInfo = myAkomando.getPublicationInfo({
          about: 'work',
        });
        expect(publicationInfo.author.name).to.equal('stato');
        expect(publicationInfo.author.ref).to.equal('/ontology/organizations/it/Stato');
        expect(publicationInfo.date).to.equal('2016-01-15');
    });

    it('The expression author is stato and publication date is 2016-01-15', function(){
        const publicationInfo = myAkomando.getPublicationInfo({
          about: 'expression',
        });
        expect(publicationInfo.author.name).to.equal('stato');
        expect(publicationInfo.author.ref).to.equal('/ontology/organizations/it/Stato');
        expect(publicationInfo.date).to.equal('2016-01-15');
        /*expect(publicationInfo.publication.date).to.equal('2016-01-22');
        expect(publicationInfo.publication.name).to.equal('GU');
        expect(publicationInfo.publication.showAs).to.equal('Gazzetta Ufficiale');
        expect(publicationInfo.publication.number).to.equal('017');*/
    });

    it('The manifestation author is cirsfid and publication date is 2016-11-13', function(){
        const publicationInfo = myAkomando.getPublicationInfo({
          about: 'manifestation',
        });
        expect(publicationInfo.author.name).to.equal('CIRSFID');
        expect(publicationInfo.author.ref).to.equal('#http://www.cirsfid.unibo.it/');
        expect(publicationInfo.date).to.equal('2016-11-13');
    });

    it('Tests with getElementById: should get 1 element "textualMod" with eId="_0"', function(){
        let elems = myAkomando.getElementById({ idValue: '_0'});
        expect(elems.length).to.equal(1);
        expect(elems[0].tagName).to.equal('textualMod');
    });

    it('Tests with getElementById: should get 1 element "container" with wId="preamble_1__preambolonir_1"', function(){
        let elems = myAkomando.getElementById({ idName: 'wId', idValue: 'preamble_1__preambolonir_1'});
        expect(elems.length).to.equal(1);
        expect(elems[0].tagName).to.equal('container');
    });

    it('Tests with getElementById: should get 0 elements with eId="preamble_1__preambolonir_1"', function(){
        let elems = myAkomando.getElementById({ idName: 'eId', idValue: 'preamble_1__preambolonir_1'});
        expect(elems).to.equal(undefined);
    });

    it('Should find 59 total id errors: 47 for eId, 12 for wId', function(){
        const totalErrors = myAkomando.checkIds();
        expect(totalErrors.length).to.equal(59);
        const eIdErrors = myAkomando.checkIds({ idName: 'eId' });
        expect(eIdErrors.length).to.equal(47);
        const wIdErrors = myAkomando.checkIds({ idName: 'wId' });
        expect(wIdErrors.length).to.equal(12);
    });
});

describe('#AkomantosoDocumentHandler Document document_uy.xml (Uruguay)', function() {
   let myAkomando;
   before((done) => {
      getFile('document_uy.xml', (content) => {
         myAkomando = createAkomando({
            aknString: content,
         });
         done();
      })
   });

   it('After having parsed the XML string the Akn Document Element must exist', function() {
      const result = myAkomando.getAkomaNtoso().documentElement.nodeType;
      expect(result).to.equal(1);
   });
   it('After having parsed the XML string the Akn Document Element must have 3 children', function() {
      const result = myAkomando.getAkomaNtoso().documentElement.childNodes.length;
      expect(result).to.equal(1);
   });
   it('After having parsed the XML string the AKNString should be long 189473 characters', function() {
      const result = myAkomando.getAkomaNtoso({
         serialize: 'AKNString',
      }).length;
      expect(result).to.equal(189473);
   });
   it('After having parsed the XML string the HTMLString should be long 223636 characters', function() {
      const result =  myAkomando.getAkomaNtoso({
         serialize: 'HTMLString',
      }).length;
      expect(result).to.equal(223636);
   });
   it('After having parsed the XML string the JSONString should be long 182267 characters', function() {
      const result = myAkomando.getAkomaNtoso({
         serialize: 'JSONString',
      });
      expect(result.length).to.equal(182267);
   });
   it('After having parsed the XML string the HTML Document Element must exist', function() {
      const result =  myAkomando.getAkomaNtoso({
         serialize: 'HTMLDom',
      }).documentElement.nodeType;
      expect(result).to.equal(1);
   });
   it('After having parsed the XML string the first element of the body must have class name equal to akn-akomaNtoso ', function() {
      const result =  myAkomando.getAkomaNtoso({
         serialize: 'HTMLDom',
      }).getElementsByTagName('body')[0].firstChild.getAttribute('class');
      expect(result).to.equal('akn-akomaNtoso');
   });
   it('After having parsed the XML string the HTML Document Element must have 2 children', function() {
      const result =  myAkomando.getAkomaNtoso({
         serialize: 'HTMLDom',
      }).documentElement.childNodes.length;
      expect(result).to.equal(2);
   });
   it('The meta in AKNDom must be unique and its name must be meta "meta"', function() {
      const meta = myAkomando.getMeta({
         serialize: 'AKNDom',
      });
      const result = meta.length;
      expect(result).to.equal(1);
      expect(meta[0].nodeName).to.equal('meta');
   });
   it('The meta in AKN string format must be long 1664 character', function() {
      const meta = myAkomando.getMeta({
         serialize: 'AKNString',
      });
      const result = meta.pop().length;
      expect(result).to.equal(1664);
   });
   it('There must be three FRBRthis elements when retreiving meta in AKN format', function() {
      const meta = myAkomando.getMeta({
         serialize: 'AKNDom',
         metaRoot: 'FRBRthis',
      });
      const result = meta.length;
      expect(result).to.equal(3);
   });
   it('There must be three FRBRthis elements when retreiving meta in JSON format', function() {
      const meta = myAkomando.getMeta({
         serialize: 'JSON',
         metaRoot: 'FRBRthis',
      });
      const result = meta.length;
      expect(result).to.equal(3);
   });
   it('The meta in JSON string format must be long 1894 character', function() {
      const meta = myAkomando.getMeta({
         serialize: 'JSONString',
      });
      const result = meta.pop().length;
      expect(result).to.equal(1894);
   });
   it('There must be three FRBRthis elements when retreiving meta in HTML format', function() {
      const meta = myAkomando.getMeta({
         serialize: 'HTMLDom',
         metaRoot: 'FRBRthis',
      });
      const result = meta.length;
      expect(result).to.equal(3);
   });
   it('The meta in HTML string format must be long 2582 character', function() {
      const meta = myAkomando.getMeta({
         serialize: 'HTMLString',
      });
      const result = meta.pop().length;
      expect(result).to.equal(2582);
   });
   it('There must be one FRBRcountry element when retreiving meta in AKNDom', function() {
      const meta = myAkomando.getMeta({
         serialize: 'AKNDom',
         metaRoot: 'FRBRcountry',
      });
      const result = meta.length;
      expect(result).to.equal(1);
   });
   it('There must be three akn:FRBRthis elements when retreiving meta in AKNDom', function() {
      const meta = myAkomando.getMeta({
         serialize: 'AKNDom',
         metaRoot: 'akn:FRBRthis',
      });
      const result = meta.length;
      expect(result).to.equal(3);
   });
   it('The document type must be bill', function() {
      const docType = myAkomando.getDocType();
      expect(docType).to.equal('bill');
   });

   it('The document must contain 374 hierarchies. The eId of the first one must be title_I and the eId of the last one must be title_IX__chp_IV__art_83__para_1', function(){
      const hiers = myAkomando.getHierIdentifiers();
      expect(hiers.length).to.equal(374);
      expect(hiers[0].eId).to.equal('title_I');
      expect(hiers[hiers.length-1].eId).to.equal('title_IX__chp_IV__art_83__para_1');
   });

   it('The document must contain 374 hierarchies when ordered descending. The eId of the first one must be title_IX__chp_IV__art_83__para_1 and the eId of the last one must be title_1', function(){
      const hiers = myAkomando.getHierIdentifiers({
         order:'descending',
      });
      expect(hiers.length).to.equal(374);
      expect(hiers[0].eId).to.equal('title_IX__chp_IV__art_83__para_1');
      expect(hiers[hiers.length-1].eId).to.equal('title_I');
   });

   it('The document must contain 374 hierarchies when sorted by name. The eId of the first one must be title_I__chp_CAP__art_1 and the eId of the last one must be title_IX', function(){
      const hiers = myAkomando.getHierIdentifiers({
         sortBy:'name',
      });
      expect(hiers.length).to.equal(374);
      expect(hiers[0].eId).to.equal('title_I__chp_CAP__art_1');
      expect(hiers[hiers.length-1].eId).to.equal('title_IX');
   });

   it('The document must contain 374 hierarchies when sorted by eId. The eId of the first one must be title_I and the eId of the last one must be title_V__chp_CAP__art_34__para_1__mod_3__qstr_1__art_3__para_1', function(){
      const hiers = myAkomando.getHierIdentifiers({
         sortBy:'eId',
      });
      expect(hiers.length).to.equal(374);
      expect(hiers[0].eId).to.equal('title_I');
      expect(hiers[hiers.length-1].eId).to.equal('title_V__chp_CAP__art_34__para_1__mod_3__qstr_1__art_3__para_1');
   });

   it('The document must contain 374 hierarchies when sorted by wId. The eId of the first one must be title_I and the eId of the last one must be title_V__chp_CAP__art_34__para_1__mod_3__qstr_1__art_3__para_1', function(){
      const hiers = myAkomando.getHierIdentifiers({
         sortBy:'wId',
      });
      expect(hiers.length).to.equal(374);
      expect(hiers[0].eId).to.equal('title_I');
      expect(hiers[hiers.length-1].eId).to.equal('title_V__chp_CAP__art_34__para_1__mod_3__qstr_1__art_3__para_1');
   });

   it('The document must contain 374 hierarchies when sorted by GUID. The eId of the first one must be title_I and the eId of the last one must be title_IX__chp_IV__art_83__para_1', function(){
      const hiers = myAkomando.getHierIdentifiers({
         sortBy:'GUID',
      });
      expect(hiers.length).to.equal(374);
      expect(hiers[0].eId).to.equal('title_I');
      expect(hiers[hiers.length-1].eId).to.equal('title_IX__chp_IV__art_83__para_1');
   });

   it('The document must contain 374 hierarchies when sorted by xpath. The eId of the first one must be title_I and the eId of the last one must be title_IX__chp_IV__art_83__para_1', function(){
      const hiers = myAkomando.getHierIdentifiers({
         sortBy:'xpath',
      });
      expect(hiers.length).to.equal(374);
      expect(hiers[0].eId).to.equal('title_I');
      expect(hiers[hiers.length-1].eId).to.equal('title_IX__chp_IV__art_83__para_1');
   });

   it('The document must contain 374 hierarchies when sorted by level. The eId of the first one must be title_I and the eId of the last one must be title_V__chp_CAP__art_32__para_1__mod_1__qstr_1__art_1__para_1__content__list_1__item_g', function(){
      const hiers = myAkomando.getHierIdentifiers({
         sortBy:'level',
      });
      expect(hiers.length).to.equal(374);
      expect(hiers[0].eId).to.equal('title_I');
      expect(hiers[hiers.length-1].eId).to.equal('title_V__chp_CAP__art_32__para_1__mod_1__qstr_1__art_1__para_1__content__list_1__item_g');
   });

   it('The document must contain 94 articles. The eId of the seventh one must be title_II__chp_CAP__art_7 and the eId of the last one must be title_IX__chp_IV__art_83', function(){
      const hiers = myAkomando.getHierIdentifiers({
         filterByName:'article',
      });
      expect(hiers.length).to.equal(94);
      expect(hiers[6].eId).to.equal('title_II__chp_CAP__art_7');
      expect(hiers[hiers.length-1].eId).to.equal('title_IX__chp_IV__art_83');
   });

   it('The document must contain 162 partitions having content. The eId of the first one must be title_I__chp_CAP__art_1__para_1 and the eId of the seventeenth one must be title_IX__chp_IV__art_83__para_1', function(){
      const hiers = myAkomando.getHierIdentifiers({
         filterByContent: true,
      });
      expect(hiers.length).to.equal(162);
      expect(hiers[0].eId).to.equal('title_I__chp_CAP__art_1__para_1');
      expect(hiers[161].eId).to.equal('title_IX__chp_IV__art_83__para_1');
   });

   it('The raw text of the document must contain 76369 characters without line breaks', function(){
      const text = myAkomando.getAkomaNtoso({ serialize: 'TEXT', newLines: false });
      expect(text.length).to.equal(76369);
   });

   it('Substring from 99 to 110 must be equal to "COS CAPÍTUL", substring from 40000 to 40020 must be equal to "ienes inmuebles part" and substring from 65000 to 65030 must be equal to " del ordenante. La instrucción"', function(){
      const text = myAkomando.getAkomaNtoso({ serialize: 'TEXT', newLines: false });
      expect(text.substring(99, 110)).to.equal('COS CAPÍTUL');
      expect(text.substring(40000, 40020)).to.equal('ienes inmuebles part');
      expect(text.substring(65000, 65030)).to.equal(' del ordenante. La instrucción');
   });

   it('The raw text of the document must contain 125041 characters with line breaks', function(){
      const text = myAkomando.getAkomaNtoso({ serialize: 'TEXT', newLines: true });
      expect(text.length).to.equal(125041);
   });

   it('The first line must be equal to "     Comisión de Hacienda   PROYECTO DE LEY19 de febrero de 2014TÍTULO IDE LOS MEDIOS DE PAGO ELECTRÓNICOS CAPÍTULO ÚNICOArtículo 1º.(Medio de pago", the last line must be equal to "                GONZÁLEZ ALEJANDRO SÁNCHEZ  " and the line "600" must be equal to "                                    probarse a través de la presentación de los recibos de depósito"', function(){
      const text = myAkomando.getAkomaNtoso({ serialize: 'TEXT', newLines: true });
      const lines = text.split('\n');
      expect(lines[0]).to.equal('     Comisión de Hacienda   PROYECTO DE LEY19 de febrero de 2014TÍTULO IDE LOS MEDIOS DE PAGO ELECTRÓNICOS CAPÍTULO ÚNICOArtículo 1º.(Medio de pago');
      expect(lines[600]).to.equal('                                    probarse a través de la presentación de los recibos de depósito');
      expect(lines[lines.length-1]).to.equal('                GONZÁLEZ ALEJANDRO SÁNCHEZ  ');
   });

   it('The total number of elements must be 1737 and element in position 17 must have name "blockList" and its count must be 17', function(){
      const elementsCount = myAkomando.countDocElements();
      expect(elementsCount.total).to.equal(1737);
      expect(elementsCount.elements[16].name).to.equal('blockList');
      expect(elementsCount.elements[16].count).to.equal(17);
   });

   it('The total number of elements ref must be 34 and element in position 0 must have name "ref" and its count must be 34', function(){
      const elementsCount = myAkomando.countDocElements({
         filterByName: 'ref',
      });
      expect(elementsCount.total).to.equal(34);
      expect(elementsCount.elements[0].name).to.equal('ref');
      expect(elementsCount.elements[0].count).to.equal(34);
   });

   it('The total number of elements mod must be 16 and element in position 0 must have name "mod" and its count must be 16', function(){
      const elementsCount = myAkomando.countDocElements({
         filterByName: 'mod',
      });
      expect(elementsCount.total).to.equal(16);
      expect(elementsCount.elements[0].name).to.equal('mod');
      expect(elementsCount.elements[0].count).to.equal(16);
   });

    it('Tests with the XPath selection', function() {
        let selection = myAkomando.getElementsFromXPath('/akomaNtoso/bill/body/title/chapter/article');
        expect(selection.length).to.equal(83);
        selection = myAkomando.getElementsFromXPath('/akomaNtoso/bill/meta/references');
        expect(selection.length).to.equal(1);
        selection = myAkomando.getElementsFromXPath('/akomaNtoso/bill/body/title[2]/heading[1]');
        expect(selection.length).to.equal(1);
    });

    it('The document\'s URIs must be: Work: "/akn/uy/bill/proyectodley/2014-02-07", Expression: "/akn/uy/bill/proyectodley/2014-02-07/spa@" and Manifestation: "/akn/uy/bill/proyectodley/2014-02-07/spa@.xml"', function() {
        const FRBR = myAkomando.getFRBR();
        expect(FRBR.work.uri).to.equal('/akn/uy/bill/proyectodley/2014-02-07');
        expect(FRBR.expression.uri).to.equal('/akn/uy/bill/proyectodley/2014-02-07/spa@');
        expect(FRBR.manifestation.uri).to.equal('/akn/uy/bill/proyectodley/2014-02-07/spa@.xml');

        expect(FRBR.work.this).to.equal('/akn/uy/bill/proyectodley/2014-02-07/!main');
        expect(FRBR.expression.this).to.equal('/akn/uy/bill/proyectodley/2014-02-07/spa@/!main');
        expect(FRBR.manifestation.this).to.equal('/akn/uy/bill/proyectodley/2014-02-07/spa@/main.xml/!main');
    });
});

describe('#AkomantosoDocumentHandler Document document_unqualified.xml (Italy and unqualified elements)', function() {
   let myAkomando;
   before((done) => {
      getFile('document_unqualified.xml', (content) => {
         myAkomando = createAkomando({
            aknString: content,
         });
         done();
      })
   });

   it('After having parsed the XML string the Akn Document Element must exist', function() {
      const result =  myAkomando.getAkomaNtoso().documentElement.nodeType;
      expect(result).to.equal(1);
   });
   it('After having parsed the XML string the Akn Document Element must have 1 children', function() {
      const result =  myAkomando.getAkomaNtoso().documentElement.childNodes.length;
      expect(result).to.equal(1);
   });
   it('After having parsed the XML string the AKNString should be long 115017 characters', function() {
      const result =  myAkomando.getAkomaNtoso({
         serialize: 'AKNString',
      }).length;
      expect(result).to.equal(115017);
   });
   it('After having parsed the XML string the HTMLString should be long 157211 characters', function() {
      const result =  myAkomando.getAkomaNtoso({
         serialize: 'HTMLString',
      }).length;
      expect(result).to.equal(157211);
   });
   it('After having parsed the XML string the JSONString should be long 118899 characters', function() {
      const result = myAkomando.getAkomaNtoso({
         serialize: 'JSONString',
      });
      expect(result.length).to.equal(118899);
   });
   it('After having parsed the XML string the HTML Document Element must exist', function() {
      const result =  myAkomando.getAkomaNtoso({
         serialize: 'HTMLDom',
      }).documentElement.nodeType;
      expect(result).to.equal(1);
   });
   it('After having parsed the XML string the first element of the body must have class name equal to akn-akomaNtoso ', function() {
      const result =  myAkomando.getAkomaNtoso({
         serialize: 'HTMLDom',
      }).getElementsByTagName('body')[0].firstChild.getAttribute('class');
      expect(result).to.equal('akn-akomaNtoso');
   });
   it('After having parsed the XML string the HTML Document Element must have 2 children', function() {
      const result =  myAkomando.getAkomaNtoso({
         serialize: 'HTMLDom',
      }).documentElement.childNodes.length;
      expect(result).to.equal(2);
   });
   it('The meta in AKNDom must be unique and its name must be meta "meta"', function() {
      const meta = myAkomando.getMeta({
         serialize: 'AKNDom',
      });
      const result = meta.length;
      expect(result).to.equal(1);
      expect(meta[0].nodeName).to.equal('meta');
   });
   it('The meta in AKN string format must be long 49665 character', function() {
      const meta = myAkomando.getMeta({
         serialize: 'AKNString',
      });
      const result = meta.pop().length;
      expect(result).to.equal(49665);
   });
   it('There must be three FRBRthis elements when retreiving meta in AKN format', function() {
      const meta = myAkomando.getMeta({
         serialize: 'AKNDom',
         metaRoot: 'FRBRthis',
      });
      const result = meta.length;
      expect(result).to.equal(3);
   });
   it('There must be three FRBRthis elements when retreiving meta in JSON format', function() {
      const meta = myAkomando.getMeta({
         serialize: 'JSON',
         metaRoot: 'FRBRthis',
      });
      const result = meta.length;
      expect(result).to.equal(3);
   });
   it('The meta in JSON string format must be long 50823 character', function() {
      const meta = myAkomando.getMeta({
         serialize: 'JSONString',
      });
      const result = meta.pop().length;
      expect(result).to.equal(50823);
   });
   it('There must be three FRBRthis elements when retreiving meta in HTML format', function() {
      const meta = myAkomando.getMeta({
         serialize: 'HTMLDom',
         metaRoot: 'FRBRthis',
      });
      const result = meta.length;
      expect(result).to.equal(3);
   });
   it('The meta in HTML string format must be long 64122 character', function() {
      const meta = myAkomando.getMeta({
         serialize: 'HTMLString',
      });
      const result = meta.pop().length;
      expect(result).to.equal(64122);
   });
   it('There must be one FRBRcountry element when retreiving meta in AKNDom', function() {
      const meta = myAkomando.getMeta({
         serialize: 'AKNDom',
         metaRoot: 'FRBRcountry',
      });
      const result = meta.length;
      expect(result).to.equal(1);
   });
   it('There must be three akn:FRBRthis elements when retreiving meta in AKNDom', function() {
      const meta = myAkomando.getMeta({
         serialize: 'AKNDom',
         metaRoot: 'akn:FRBRthis',
      });
      const result = meta.length;
      expect(result).to.equal(3);
   });
   it('The document type must be act', function() {
      const docType = myAkomando.getDocType();
      expect(docType).to.equal('act');
   });

   it('The document must contain 162 hierarchies. The eId of the first one must be art_1 and the eId of the last one must be art_10__para_1', function(){
      const hiers = myAkomando.getHierIdentifiers();
      expect(hiers.length).to.equal(162);
      expect(hiers[0].eId).to.equal('art_1');
      expect(hiers[hiers.length-1].eId).to.equal('art_10__para_1');
   });

   it('The document must contain 162 hierarchies when ordered descending. The eId of the first one must be art_10__para_1 and the eId of the last one must be art_10', function(){
      const hiers = myAkomando.getHierIdentifiers({
         order:'descending',
      });
      expect(hiers.length).to.equal(162);
      expect(hiers[0].eId).to.equal('art_10__para_1');
      expect(hiers[hiers.length-1].eId).to.equal('art_1');
   });

   it('The document must contain 162 hierarchies when sorted by name. The eId of the first one must be art_1 and the eId of the last one must be art_3__para_7__mod_27__qstr_2', function(){
      const hiers = myAkomando.getHierIdentifiers({
         sortBy:'name',
      });
      expect(hiers.length).to.equal(162);
      expect(hiers[0].eId).to.equal('art_1');
      expect(hiers[hiers.length-1].eId).to.equal('art_3__para_7__mod_27__qstr_2');
   });

   it('The document must contain 162 hierarchies when sorted by eId. The eId of the first one must be art_1 and the eId of the last one must be art_9__para_6', function(){
      const hiers = myAkomando.getHierIdentifiers({
         sortBy:'eId',
      });
      expect(hiers.length).to.equal(162);
      expect(hiers[0].eId).to.equal('art_1');
      expect(hiers[hiers.length-1].eId).to.equal('art_9__para_6');
   });

   it('The document must contain 162 hierarchies when sorted by wId. The eId of the first one must be art_1 and the eId of the last one must be art_3__para_6__mod_26__qstr_1__para_1-bis', function(){
      const hiers = myAkomando.getHierIdentifiers({
         sortBy:'wId',
      });
      expect(hiers.length).to.equal(162);
      expect(hiers[0].eId).to.equal('art_1');
      expect(hiers[hiers.length-1].eId).to.equal('art_3__para_6__mod_26__qstr_1__para_1-bis');
   });

   it('The document must contain 162 hierarchies when sorted by GUID. The eId of the first one must be art_1 and the eId of the last one must be art_10__para_1', function(){
      const hiers = myAkomando.getHierIdentifiers({
         sortBy:'GUID',
      });
      expect(hiers.length).to.equal(162);
      expect(hiers[0].eId).to.equal('art_1');
      expect(hiers[hiers.length-1].eId).to.equal('art_10__para_1');
   });

   it('The document must contain 162 hierarchies when sorted by xpath. The eId of the first one must be art_10 and the eId of the last one must be art_9__para_6', function(){
      const hiers = myAkomando.getHierIdentifiers({
         sortBy:'xpath',
      });
      expect(hiers.length).to.equal(162);
      expect(hiers[0].eId).to.equal('art_10');
      expect(hiers[hiers.length-1].eId).to.equal('art_9__para_6');
   });

   it('The document must contain 162 hierarchies when sorted by level. The eId of the first one must be art_1 and the eId of the last one must be art_3__para_1__list_1__point_b__list_1__point_2__mod_17__qstr_1__para_1', function(){
      const hiers = myAkomando.getHierIdentifiers({
         sortBy:'level',
      });
      expect(hiers.length).to.equal(162);
      expect(hiers[0].eId).to.equal('art_1');
      expect(hiers[hiers.length-1].eId).to.equal('art_3__para_1__list_1__point_b__list_1__point_2__mod_17__qstr_1__para_1');
   });

   it('The document must contain 10 articles. The eId of the seventh one must be art_7 and the eId of the last one must be art_10', function(){
      const hiers = myAkomando.getHierIdentifiers({
         filterByName: 'article',
      });
      expect(hiers.length).to.equal(10);
      expect(hiers[6].eId).to.equal('art_7');
      expect(hiers[hiers.length-1].eId).to.equal('art_10');
   });

   it('The document must contain 58 partitions having content. The eId of the first one must be art_1__para_1 and the eId of the seventeenth one must be art_2__para_5__list_1__point_a', function(){
      const hiers = myAkomando.getHierIdentifiers({
         filterByContent: true,
      });
      expect(hiers.length).to.equal(58);
      expect(hiers[0].eId).to.equal('art_1__para_1');
      expect(hiers[57].eId).to.equal('art_10__para_1');
   });

   it('The raw text of the document must contain 25020 characters without line breaks', function(){
      const text = myAkomando.getAkomaNtoso({ serialize: 'TEXT', newLines: false });
      expect(text.length).to.equal(25020);
   });

   it('Substring from 99 to 110 must be equal to "articolo 2,", substring from 14995 to 15020 must be equal to "le seguenti: \"Si fa luogo" and substring from 20000 to 20030 must be equal to "ente a ricevere il rapporto e "', function(){
      const text = myAkomando.getAkomaNtoso({ serialize: 'TEXT', newLines: false });
      expect(text.substring(99, 110)).to.equal('articolo 2,');
      expect(text.substring(14995, 15020)).to.equal('le seguenti: "Si fa luogo');
      expect(text.substring(20000, 20030)).to.equal('ente a ricevere il rapporto e ');
   });

   it('The raw text of the document must contain 33261 characters with line breaks', function(){
      const text = myAkomando.getAkomaNtoso({ serialize: 'TEXT', newLines: true });
      expect(text.length).to.equal(33261);
   });

   it('The first line must be equal to "DECRETO LEGISLATIVO15 gennaio 2016, n. ", the last line must be equal to "                        6.Il pagamento determina ... il Guardasigilli: Orlando" and the line "170" must be equal to "                                        "è punito con l\'arresto da uno a sei mesi ovvero con l\'ammenda da euro 30 a euro 619""', function(){
      const text = myAkomando.getAkomaNtoso({ serialize: 'TEXT', newLines: true });
      const lines = text.split('\n');
      expect(lines[0]).to.equal('DECRETO LEGISLATIVO15 gennaio 2016, n. ');
      expect(lines[170]).to.equal('                                        "è punito con l\'arresto da uno a sei mesi ovvero con l\'ammenda da euro 30 a euro 619"');
      expect(lines[lines.length-1]).to.equal('                        6.Il pagamento determina l\'estinzione del procedimento.Art. 10Disposizioni finanziarie1.Le amministrazioni interessate provvedono agli adempimenti previsti dal presente decreto, senza nuovi o maggiori oneri a carico della finanza pubblica, con le risorse umane, strumentali e finanziarie disponibili a legislazione vigente.Il presente decreto, munito del sigillo dello Stato, sarà inserito nella Raccolta ufficiale degli atti normativi della Repubblica Italiana. È fatto obbligo a chiunque spetti di osservarlo e di farlo osservare.Dato a Roma, addì 15 gennaio 2016MATTARELLARenzi, Presidente del Consiglio dei ministriOrlando, Ministro della giustiziaPadoan, Ministro dell\'economia e delle finanzeVisto, il Guardasigilli: Orlando');
   });

   it('The total number of elements must be 1697 and element in position 17 must have name "akomaNtoso" and its count must be 1', function(){
      const elementsCount = myAkomando.countDocElements();
      expect(elementsCount.total).to.equal(1697);
      expect(elementsCount.elements[16].name).to.equal('akomaNtoso');
      expect(elementsCount.elements[16].count).to.equal(1);
   });

   it('The total number of elements ref must be 97 and element in position 0 must have name "ref" and its count must be 97', function(){
      const elementsCount = myAkomando.countDocElements({
         filterByName: 'ref',
      });
      expect(elementsCount.total).to.equal(97);
      expect(elementsCount.elements[0].name).to.equal('ref');
      expect(elementsCount.elements[0].count).to.equal(97);
   });

   it('The total number of elements mod must be 27 and element in position 0 must have name "mod" and its count must be 27', function(){
      const elementsCount = myAkomando.countDocElements({
         filterByName: 'mod',
      });
      expect(elementsCount.total).to.equal(27);
      expect(elementsCount.elements[0].name).to.equal('mod');
      expect(elementsCount.elements[0].count).to.equal(27);
   });
});

describe('#AkomantosoDocumentHandler Document document_qualifiedWithPrefix.xml (Italy, qualified and prefixed elements)', function() {
   let myAkomando;
   before((done) => {
      getFile('document_qualifiedWithPrefix.xml', (content) => {
         myAkomando = createAkomando({
            aknString: content,
         });
         done();
      })
   });

   it('After having parsed the XML string the Akn Document Element must exist', function() {
      const result =  myAkomando.getAkomaNtoso().documentElement.nodeType;
      expect(result).to.equal(1);
   });
   it('After having parsed the XML string the Akn Document Element must have 1 children', function() {
      const result =  myAkomando.getAkomaNtoso().documentElement.childNodes.length;
      expect(result).to.equal(1);
   });
   it('After having parsed the XML string the AKNString should be long 121803 characters', function() {
      const result =  myAkomando.getAkomaNtoso({
         serialize: 'AKNString',
      }).length;
      expect(result).to.equal(121803);
   });
   it('After having parsed the XML string the HTMLString should be long 161474 characters', function() {
      const result =  myAkomando.getAkomaNtoso({
         serialize: 'HTMLString',
      }).length;
      expect(result).to.equal(161474);
   });
   it('After having parsed the XML string the JSONString should be long 121483 characters', function() {
      const result = myAkomando.getAkomaNtoso({
         serialize: 'JSONString',
      });
      expect(result.length).to.equal(121483);
   });
   it('After having parsed the XML string the HTML Document Element must exist', function() {
      const result =  myAkomando.getAkomaNtoso({
         serialize: 'HTMLDom',
      }).documentElement.nodeType;
      expect(result).to.equal(1);
   });
   it('After having parsed the XML string the first element of the body must have class name equal to akn-akomaNtoso ', function() {
      const result =  myAkomando.getAkomaNtoso({
         serialize: 'HTMLDom',
      }).getElementsByTagName('body')[0].firstChild.getAttribute('class');
      expect(result).to.equal('akn-akn:akomaNtoso');
   });
   it('After having parsed the XML string the HTML Document Element must have 2 children', function() {
      const result =  myAkomando.getAkomaNtoso({
         serialize: 'HTMLDom',
      }).documentElement.childNodes.length;
      expect(result).to.equal(2);
   });
   it('The meta in AKNDom must be unique and its name must be meta "meta"', function() {
      const meta = myAkomando.getMeta({
         serialize: 'AKNDom',
      });
      const result = meta.length;
      expect(result).to.equal(1);
      expect(meta[0].nodeName).to.equal('akn:meta');
   });
   it('The meta in AKN string format must be long 51511 character', function() {
      const meta = myAkomando.getMeta({
         serialize: 'AKNString',
      });
      const result = meta.pop().length;
      expect(result).to.equal(51511);
   });
   it('There must be three FRBRthis elements when retreiving meta in AKN format', function() {
      const meta = myAkomando.getMeta({
         serialize: 'AKNDom',
         metaRoot: 'FRBRthis',
      });
      const result = meta.length;
      expect(result).to.equal(3);
   });
   it('There must be three FRBRthis elements when retreiving meta in JSON format', function() {
      const meta = myAkomando.getMeta({
         serialize: 'JSON',
         metaRoot: 'FRBRthis',
      });
      const result = meta.length;
      expect(result).to.equal(3);
   });
   it('The meta in JSON string format must be long 51509 character', function() {
      const meta = myAkomando.getMeta({
         serialize: 'JSONString',
      });
      const result = meta.pop().length;
      expect(result).to.equal(51509);
   });
   it('There must be three FRBRthis elements when retreiving meta in HTML format', function() {
      const meta = myAkomando.getMeta({
         serialize: 'HTMLDom',
         metaRoot: 'FRBRthis',
      });
      const result = meta.length;
      expect(result).to.equal(3);
   });
   it('The meta in HTML string format must be long 65678 character', function() {
      const meta = myAkomando.getMeta({
         serialize: 'HTMLString',
      });
      const result = meta.pop().length;
      expect(result).to.equal(65678);
   });
   it('There must be one FRBRcountry element when retreiving meta in AKNDom', function() {
      const meta = myAkomando.getMeta({
         serialize: 'AKNDom',
         metaRoot: 'FRBRcountry',
      });
      const result = meta.length;
      expect(result).to.equal(1);
   });
   it('There must be three akn:FRBRthis elements when retreiving meta in AKNDom', function() {
      const meta = myAkomando.getMeta({
         serialize: 'AKNDom',
         metaRoot: 'akn:FRBRthis',
      });
      const result = meta.length;
      expect(result).to.equal(3);
   });
   it('The document type must be act', function() {
      const docType = myAkomando.getDocType();
      expect(docType).to.equal('act');
   });

   it('The document must contain 162 hierarchies. The eId of the first one must be art_1 and the eId of the last one must be art_10__para_1', function(){
      const hiers = myAkomando.getHierIdentifiers();
      expect(hiers.length).to.equal(162);
      expect(hiers[0].eId).to.equal('art_1');
      expect(hiers[hiers.length-1].eId).to.equal('art_10__para_1');
   });

   it('The document must contain 162 hierarchies when ordered descending. The eId of the first one must be art_10__para_1 and the eId of the last one must be art_10', function(){
      const hiers = myAkomando.getHierIdentifiers({
         order:'descending',
      });
      expect(hiers.length).to.equal(162);
      expect(hiers[0].eId).to.equal('art_10__para_1');
      expect(hiers[hiers.length-1].eId).to.equal('art_1');
   });

   it('The document must contain 162 hierarchies when sorted by name. The eId of the first one must be art_1 and the eId of the last one must be art_3__para_7__mod_27__qstr_2', function(){
      const hiers = myAkomando.getHierIdentifiers({
         sortBy:'name',
      });
      expect(hiers.length).to.equal(162);
      expect(hiers[0].eId).to.equal('art_1');
      expect(hiers[hiers.length-1].eId).to.equal('art_3__para_7__mod_27__qstr_2');
   });

   it('The document must contain 162 hierarchies when sorted by eId. The eId of the first one must be art_1 and the eId of the last one must be art_9__para_6', function(){
      const hiers = myAkomando.getHierIdentifiers({
         sortBy:'eId',
      });
      expect(hiers.length).to.equal(162);
      expect(hiers[0].eId).to.equal('art_1');
      expect(hiers[hiers.length-1].eId).to.equal('art_9__para_6');
   });

   it('The document must contain 162 hierarchies when sorted by wId. The eId of the first one must be art_1 and the eId of the last one must be art_3__para_6__mod_26__qstr_1__para_1-bis', function(){
      const hiers = myAkomando.getHierIdentifiers({
         sortBy:'wId',
      });
      expect(hiers.length).to.equal(162);
      expect(hiers[0].eId).to.equal('art_1');
      expect(hiers[hiers.length-1].eId).to.equal('art_3__para_6__mod_26__qstr_1__para_1-bis');
   });

   it('The document must contain 162 hierarchies when sorted by GUID. The eId of the first one must be art_1 and the eId of the last one must be art_10__para_1', function(){
      const hiers = myAkomando.getHierIdentifiers({
         sortBy:'GUID',
      });
      expect(hiers.length).to.equal(162);
      expect(hiers[0].eId).to.equal('art_1');
      expect(hiers[hiers.length-1].eId).to.equal('art_10__para_1');
   });

   it('The document must contain 162 hierarchies when sorted by xpath. The eId of the first one must be art_10 and the eId of the last one must be art_9__para_6', function(){
      const hiers = myAkomando.getHierIdentifiers({
         sortBy:'xpath',
      });
      expect(hiers.length).to.equal(162);
      expect(hiers[0].eId).to.equal('art_10');
      expect(hiers[hiers.length-1].eId).to.equal('art_9__para_6');
   });

   it('The document must contain 162 hierarchies when sorted by level. The eId of the first one must be art_1 and the eId of the last one must be art_3__para_1__list_1__point_b__list_1__point_2__mod_17__qstr_1__para_1', function(){
      const hiers = myAkomando.getHierIdentifiers({
         sortBy:'level',
      });
      expect(hiers.length).to.equal(162);
      expect(hiers[0].eId).to.equal('art_1');
      expect(hiers[hiers.length-1].eId).to.equal('art_3__para_1__list_1__point_b__list_1__point_2__mod_17__qstr_1__para_1');
   });

   it('The document must contain 10 articles. The eId of the seventh one must be art_7 and the eId of the last one must be art_10', function(){
      const hiers = myAkomando.getHierIdentifiers({
         filterByName: 'article',
      });
      expect(hiers.length).to.equal(10);
      expect(hiers[6].eId).to.equal('art_7');
      expect(hiers[hiers.length-1].eId).to.equal('art_10');
   });

   it('The document must contain 58 partitions having content. The eId of the first one must be art_1__para_1 and the eId of the seventeenth one must be art_2__para_5__list_1__point_a', function(){
      const hiers = myAkomando.getHierIdentifiers({
         filterByContent: true,
      });
      expect(hiers.length).to.equal(58);
      expect(hiers[0].eId).to.equal('art_1__para_1');
      expect(hiers[57].eId).to.equal('art_10__para_1');
   });

   it('The raw text of the document must contain 25020 characters without line breaks', function(){
      const text = myAkomando.getAkomaNtoso({ serialize: 'TEXT', newLines: false });
      expect(text.length).to.equal(25020);
   });

   it('Substring from 99 to 110 must be equal to "articolo 2,", substring from 14995 to 15020 must be equal to "le seguenti: \"Si fa luogo" and substring from 20000 to 20030 must be equal to "ente a ricevere il rapporto e "', function(){
      const text = myAkomando.getAkomaNtoso({ serialize: 'TEXT', newLines: false });
      expect(text.substring(99, 110)).to.equal('articolo 2,');
      expect(text.substring(14995, 15020)).to.equal('le seguenti: "Si fa luogo');
      expect(text.substring(20000, 20030)).to.equal('ente a ricevere il rapporto e ');
   });

   it('The raw text of the document must contain 33261 characters with line breaks', function(){
      const text = myAkomando.getAkomaNtoso({ serialize: 'TEXT', newLines: true });
      expect(text.length).to.equal(33261);
   });

   it('The first line must be equal to "DECRETO LEGISLATIVO15 gennaio 2016, n. ", the last line must be equal to "                        6.Il pagamento determina ... il Guardasigilli: Orlando" and the line "170" must be equal to "                                        "è punito con l\'arresto da uno a sei mesi ovvero con l\'ammenda da euro 30 a euro 619""', function(){
      const text = myAkomando.getAkomaNtoso({ serialize: 'TEXT', newLines: true });
      const lines = text.split('\n');
      expect(lines[0]).to.equal('DECRETO LEGISLATIVO15 gennaio 2016, n. ');
      expect(lines[170]).to.equal('                                        "è punito con l\'arresto da uno a sei mesi ovvero con l\'ammenda da euro 30 a euro 619"');
      expect(lines[lines.length-1]).to.equal('                        6.Il pagamento determina l\'estinzione del procedimento.Art. 10Disposizioni finanziarie1.Le amministrazioni interessate provvedono agli adempimenti previsti dal presente decreto, senza nuovi o maggiori oneri a carico della finanza pubblica, con le risorse umane, strumentali e finanziarie disponibili a legislazione vigente.Il presente decreto, munito del sigillo dello Stato, sarà inserito nella Raccolta ufficiale degli atti normativi della Repubblica Italiana. È fatto obbligo a chiunque spetti di osservarlo e di farlo osservare.Dato a Roma, addì 15 gennaio 2016MATTARELLARenzi, Presidente del Consiglio dei ministriOrlando, Ministro della giustiziaPadoan, Ministro dell\'economia e delle finanzeVisto, il Guardasigilli: Orlando');
   });

   it('The total number of elements must be 1697 and element in position 17 must have name "akn:akomaNtoso" and its count must be 1', function(){
      const elementsCount = myAkomando.countDocElements();
      expect(elementsCount.total).to.equal(1697);
      expect(elementsCount.elements[16].name).to.equal('akn:akomaNtoso');
      expect(elementsCount.elements[16].count).to.equal(1);
   });

   it('The total number of elements akn:ref must be 97 and element in position 0 must have name "akn:ref" and its count must be 97', function(){
      const elementsCount = myAkomando.countDocElements({
         filterByName: 'akn:ref',
      });
      expect(elementsCount.total).to.equal(97);
      expect(elementsCount.elements[0].name).to.equal('akn:ref');
      expect(elementsCount.elements[0].count).to.equal(97);
   });

   it('The total number of elements akn:mod must be 27 and element in position 0 must have name "akn:mod" and its count must be 27', function(){
      const elementsCount = myAkomando.countDocElements({
         filterByName: 'akn:mod',
      });
      expect(elementsCount.total).to.equal(27);
      expect(elementsCount.elements[0].name).to.equal('akn:mod');
      expect(elementsCount.elements[0].count).to.equal(27);
   });

    it('Tests with the XPath selection', function() {
        let selection = myAkomando.getElementsFromXPath('/akn:akomaNtoso/akn:act/akn:body/akn:article');
        expect(selection.length).to.equal(10);
        selection = myAkomando.getElementsFromXPath('/akn:akomaNtoso/akn:bill');
        expect(selection.length).to.equal(0);
        selection = myAkomando.getElementsFromXPath('/akn:akomaNtoso/akn:act/akn:conclusions[1]/akn:p/akn:signature');
        expect(selection.length).to.equal(5);
    });

    it('The document\'s URIs must be: Work: "/akn/it/act/decretolegislativo/stato/2016-01-15/decreto legislativo", Expression: "/akn/it/act/decretolegislativo/stato/2016-01-15/decreto legislativo/ita@" and Manifestation: "/akn/it/act/decretolegislativo/stato/2016-01-15/decreto legislativo/ita@.xml"', function() {
        const FRBR = myAkomando.getFRBR();
        expect(FRBR.work.uri).to.equal('/akn/it/act/decretolegislativo/stato/2016-01-15/decreto legislativo');
        expect(FRBR.expression.uri).to.equal('/akn/it/act/decretolegislativo/stato/2016-01-15/decreto legislativo/ita@');
        expect(FRBR.manifestation.uri).to.equal('/akn/it/act/decretolegislativo/stato/2016-01-15/decreto legislativo/ita@.xml');

        expect(FRBR.work.this).to.equal('/akn/it/act/decretolegislativo/stato/2016-01-15/decreto legislativo/!main');
        expect(FRBR.expression.this).to.equal('/akn/it/act/decretolegislativo/stato/2016-01-15/decreto legislativo/ita@/!main');
        expect(FRBR.manifestation.this).to.equal('/akn/it/act/decretolegislativo/stato/2016-01-15/decreto legislativo/ita@/main.xml/!main');
    });
});

describe('#AkomantosoDocumentHandler Document document_docCollection.xml (Uruguaian qualified document collection)', function() {
   let myAkomando;
   before((done) => {
      getFile('document_docCollection.xml', (content) => {
         myAkomando = createAkomando({
            aknString: content,
         });
         done();
      })
   });

   it('After having parsed the XML string the Akn Document Element must exist', function() {
      const result =  myAkomando.getAkomaNtoso().documentElement.nodeType;
      expect(result).to.equal(1);
   });
   it('After having parsed the XML string the Akn Document Element must have 2 children', function() {
      const result =  myAkomando.getAkomaNtoso().documentElement.childNodes.length;
      expect(result).to.equal(2);
   });
   it('After having parsed the XML string the AKNString should be long 53303 characters', function() {
      const result = myAkomando.getAkomaNtoso({
         serialize: 'AKNString',
      }).length;
      expect(result).to.equal(53303);
   });
   it('After having parsed the XML string the HTMLString should be long 58531 characters', function() {
      const result =  myAkomando.getAkomaNtoso({
         serialize: 'HTMLString',
      }).length;
      expect(result).to.equal(58531);
   });
   it('After having parsed the XML string the JSONString should be long 46948 characters', function() {
      const result = myAkomando.getAkomaNtoso({
         serialize: 'JSONString',
      });
      expect(result.length).to.equal(46948);
   });
   it('After having parsed the XML string the HTML Document Element must exist', function() {
      const result =  myAkomando.getAkomaNtoso({
         serialize: 'HTMLDom',
      }).documentElement.nodeType;
      expect(result).to.equal(1);
   });
   it('After having parsed the XML string the first element of the body must have class name equal to akn-akomaNtoso ', function() {
      const result =  myAkomando.getAkomaNtoso({
         serialize: 'HTMLDom',
      }).getElementsByTagName('body')[0].firstChild.getAttribute('class');
      expect(result).to.equal('akn-akomaNtoso');
   });
   it('After having parsed the XML string the HTML Document Element must have 2 children', function() {
      const result =  myAkomando.getAkomaNtoso({
         serialize: 'HTMLDom',
      }).documentElement.childNodes.length;
      expect(result).to.equal(2);
   });
   it('The must be three meta in AKNDom and their names must be "meta"', function() {
      const meta = myAkomando.getMeta({
         serialize: 'AKNDom',
      });
      const result = meta.length;
      expect(result).to.equal(3);
      expect(meta[0].nodeName).to.equal('meta');
      expect(meta[1].nodeName).to.equal('meta');
      expect(meta[2].nodeName).to.equal('meta');
   });
   it('The firs meta block in AKN string format must be long 1854 character', function() {
      const meta = myAkomando.getMeta({
         serialize: 'AKNString',
      });
      const result = meta.pop().length;
      expect(result).to.equal(1854);
   });
   it('There must be nine FRBRthis elements when retreiving meta in AKN format', function() {
      const meta = myAkomando.getMeta({
         serialize: 'AKNDom',
         metaRoot: 'FRBRthis',
      });
      const result = meta.length;
      expect(result).to.equal(9);
   });
   it('There must be nine FRBRthis elements when retreiving meta in JSON format', function() {
      const meta = myAkomando.getMeta({
         serialize: 'JSON',
         metaRoot: 'FRBRthis',
      });
      const result = meta.length;
      expect(result).to.equal(9);
   });
   it('The first meta block in JSON string format must be long 2064 character', function() {
      const meta = myAkomando.getMeta({
         serialize: 'JSONString',
      });
      const result = meta.pop().length;
      expect(result).to.equal(2064);
   });
   it('There must be nine FRBRthis elements when retreiving meta in HTML format', function() {
      const meta = myAkomando.getMeta({
         serialize: 'HTMLDom',
         metaRoot: 'FRBRthis',
      });
      const result = meta.length;
      expect(result).to.equal(9);
   });
   it('The first meta block in HTML string format must be long 2772 character', function() {
      const meta = myAkomando.getMeta({
         serialize: 'HTMLString',
      });
      const result = meta.pop().length;
      expect(result).to.equal(2772);
   });
   it('There must be three FRBRcountry elements when retreiving meta in AKNDom', function() {
      const meta = myAkomando.getMeta({
         serialize: 'AKNDom',
         metaRoot: 'FRBRcountry',
      });
      const result = meta.length;
      expect(result).to.equal(3);
   });
   it('There must be nine akn:FRBRthis elements when retreiving meta in AKNDom', function() {
      const meta = myAkomando.getMeta({
         serialize: 'AKNDom',
         metaRoot: 'akn:FRBRthis',
      });
      const result = meta.length;
      expect(result).to.equal(9);
   });
   it('The document type must be documentCollection', function() {
      const docType = myAkomando.getDocType();
      expect(docType).to.equal('documentCollection');
   });

   it('The document must contain 59 hierarchies. The eId of the first one must be cmpnts__cmp_1__art_1 and the eId of the last one must be cmpnts__cmp_2__para_1', function(){
      const hiers = myAkomando.getHierIdentifiers();
      expect(hiers.length).to.equal(59);
      expect(hiers[0].eId).to.equal('cmpnts__cmp_1__art_1');
      expect(hiers[hiers.length-1].eId).to.equal('cmpnts__cmp_2__para_1');
   });

   it('The document must contain 59 hierarchies when ordered descending. The eId of the first one must be cmpnts__cmp_2__para_1 and the eId of the last one must be cmpnts__cmp_1__art_1', function(){
      const hiers = myAkomando.getHierIdentifiers({
         order:'descending',
      });
      expect(hiers.length).to.equal(59);
      expect(hiers[0].eId).to.equal('cmpnts__cmp_2__para_1');
      expect(hiers[hiers.length-1].eId).to.equal('cmpnts__cmp_1__art_1');
   });

   it('The document must contain 59 hierarchies when sorted by name. The eId of the first one must be cmpnts__cmp_1__art_1 and the eId of the last one must be cmpnts__cmp_1__art_7__para_1__mod_7__qstr_1', function(){
      const hiers = myAkomando.getHierIdentifiers({
         sortBy:'name',
      });
      expect(hiers.length).to.equal(59);
      expect(hiers[0].eId).to.equal('cmpnts__cmp_1__art_1');
      expect(hiers[hiers.length-1].eId).to.equal('cmpnts__cmp_1__art_7__para_1__mod_7__qstr_1');
   });

   it('The document must contain 59 hierarchies when sorted by eId. The eId of the first one must be cmpnts__cmp_1__art_1 and the eId of the last one must be cmpnts__cmp_2__para_1', function(){
      const hiers = myAkomando.getHierIdentifiers({
         sortBy:'eId',
      });
      expect(hiers.length).to.equal(59);
      expect(hiers[0].eId).to.equal('cmpnts__cmp_1__art_1');
      expect(hiers[hiers.length-1].eId).to.equal('cmpnts__cmp_2__para_1');
   });

   it('The document must contain 59 hierarchies when sorted by wId. The eId of the first one must be cmpnts__cmp_1__art_3__para_1__mod_3 and the eId of the last one must be cmpnts__cmp_2__para_1', function(){
      const hiers = myAkomando.getHierIdentifiers({
         sortBy:'wId',
      });
      expect(hiers.length).to.equal(59);
      expect(hiers[0].eId).to.equal('cmpnts__cmp_1__art_3__para_1__mod_3');
      expect(hiers[hiers.length-1].eId).to.equal('cmpnts__cmp_2__para_1');
   });

   it('The document must contain 59 hierarchies when sorted by GUID. The eId of the first one must be cmpnts__cmp_1__art_1 and the eId of the last one must be cmpnts__cmp_2__para_1', function(){
      const hiers = myAkomando.getHierIdentifiers({
         sortBy:'GUID',
      });
      expect(hiers.length).to.equal(59);
      expect(hiers[0].eId).to.equal('cmpnts__cmp_1__art_1');
      expect(hiers[hiers.length-1].eId).to.equal('cmpnts__cmp_2__para_1');
   });

   it('The document must contain 59 hierarchies when sorted by xpath. The eId of the first one must be cmpnts__cmp_1__art_1 and the eId of the last one must be cmpnts__cmp_2__para_1', function(){
      const hiers = myAkomando.getHierIdentifiers({
         sortBy:'xpath',
      });
      expect(hiers.length).to.equal(59);
      expect(hiers[0].eId).to.equal('cmpnts__cmp_1__art_1');
      expect(hiers[hiers.length-1].eId).to.equal('cmpnts__cmp_2__para_1');
   });

   it('The document must contain 59 hierarchies when sorted by level. The eId of the first one must be cmpnts__cmp_1__art_1 and the eId of the last one must be cmpnts__cmp_1__art_2__para_1__mod_2__qstr_1__art_2__para_2__list_1__item_C', function(){
      const hiers = myAkomando.getHierIdentifiers({
         sortBy:'level',
      });
      expect(hiers.length).to.equal(59);
      expect(hiers[0].eId).to.equal('cmpnts__cmp_1__art_1');
      expect(hiers[hiers.length-1].eId).to.equal('cmpnts__cmp_1__art_2__para_1__mod_2__qstr_1__art_2__para_2__list_1__item_C');
   });

   it('The document must contain 13 articles. The eId of the seventh one must be cmpnts__cmp_1__art_4__para_1__mod_4__qstr_1__art_7 and the eId of the last one must be cmpnts__cmp_1__art_7__para_1__mod_7__qstr_1__art_19', function(){
      const hiers = myAkomando.getHierIdentifiers({
         filterByName: 'article',
      });
      expect(hiers.length).to.equal(13);
      expect(hiers[6].eId).to.equal('cmpnts__cmp_1__art_4__para_1__mod_4__qstr_1__art_7');
      expect(hiers[hiers.length-1].eId).to.equal('cmpnts__cmp_1__art_7__para_1__mod_7__qstr_1__art_19');
   });

   it('The document must contain 29 partitions having content. The eId of the first one must be cmpnts__cmp_1__art_1__para_1 and the eId of the seventeenth one must be cmpnts__cmp_2__para_1', function(){
      const hiers = myAkomando.getHierIdentifiers({
         filterByContent: true,
      });
      expect(hiers.length).to.equal(29);
      expect(hiers[0].eId).to.equal('cmpnts__cmp_1__art_1__para_1');
      expect(hiers[28].eId).to.equal('cmpnts__cmp_2__para_1');
   });

   it('The raw text of the document must contain 14728 characters without line breaks', function(){
      const text = myAkomando.getAkomaNtoso({ serialize: 'TEXT', newLines: false });
      expect(text.length).to.equal(14728);
   });

   it('Substring from 99 to 110 must be equal to "ulo 1º.- Mo", substring from 10000 to 10020 must be equal to "s fueron positivas, " and substring from 14000 to 14030 must be equal to "sona determinada este proyecto"', function(){
      const text = myAkomando.getAkomaNtoso({ serialize: 'TEXT', newLines: false });
      expect(text.substring(99, 110)).to.equal('ulo 1º.- Mo');
      expect(text.substring(10000, 10020)).to.equal('s fueron positivas, ');
      expect(text.substring(14000, 14030)).to.equal('sona determinada este proyecto');
   });

   it('The raw text of the document must contain 26311 characters with line breaks', function(){
      const text = myAkomando.getAkomaNtoso({ serialize: 'TEXT', newLines: true });
      expect(text.length).to.equal(26311);
   });

   it('The first line must be equal to "EXTRACCION, CONSERVACION Y TRASPLANTES DE ORGANOS Y TEJIDOS", the last line must be equal to "                         por Canelones " and the line "160" must be equal to "                                                  llevado por cada establecimiento asistencial"', function(){
      const text = myAkomando.getAkomaNtoso({ serialize: 'TEXT', newLines: true });
      const lines = text.split('\n');
      expect(lines[0]).to.equal('EXTRACCION, CONSERVACION Y TRASPLANTES DE ORGANOS Y TEJIDOS');
      expect(lines[130]).to.equal('                                                  llevado por cada establecimiento asistencial');
      expect(lines[lines.length-1]).to.equal('                         por Canelones ');
   });

   it('The total number of elements must be 451 and element in position 451 must have name "article" and its count must be 13', function(){
      const elementsCount = myAkomando.countDocElements();
      expect(elementsCount.total).to.equal(451);
      expect(elementsCount.elements[16].name).to.equal('article');
      expect(elementsCount.elements[16].count).to.equal(13);
   });

   it('The total number of elements ref must be 11 and element in position 0 must have name "ref" and its count must be 11', function(){
      const elementsCount = myAkomando.countDocElements({
         filterByName: 'ref',
      });
      expect(elementsCount.total).to.equal(11);
      expect(elementsCount.elements[0].name).to.equal('ref');
      expect(elementsCount.elements[0].count).to.equal(11);
   });

   it('The total number of elements mod must be 7 and element in position 0 must have name "mod" and its count must be 7', function(){
      const elementsCount = myAkomando.countDocElements({
         filterByName: 'mod',
      });
      expect(elementsCount.total).to.equal(7);
      expect(elementsCount.elements[0].name).to.equal('mod');
      expect(elementsCount.elements[0].count).to.equal(7);
   });
});
