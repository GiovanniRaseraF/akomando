// libs
import { typeCheck } from 'type-check';

// classes
import _DocumentHandler from '../classes/document-handler.js';

// configs
import aknConfig from '../config/config.json';

// errors
import errors from '../config/errors.json';

/**
 * This is the akomando api. Currently the API supports the following features:
 * @protected
*/
class Akomando {
   /**
     * Object schema for the akomando configuration
     * @public
     * @typedef {Object} akomando.config
     * @property {String} akomaNtosoRoot The name of the root of AkomaNtoso documents
     * @property {Object} docTypes Properties of AkomaNtoso doctypes
     * @property {Object} docTypes.amendmentList Properties of the amendmentList doctype of AkomaNtoso
     * @property {String} docTypes.amendmentList.name The name of the amendmentList doctype
     * @property {Object} docTypes.officialGazette Properties of the officialGazette doctype of AkomaNtoso
     * @property {String} docTypes.officialGazette.name The name of the officialGazette doctype
     * @property {Object} docTypes.documentCollection Properties of the documentCollection doctype of AkomaNtoso
     * @property {String} docTypes.documentCollection.name The name of the documentCollection doctype
     * @property {Object} docTypes.act Properties of the act doctype of AkomaNtoso
     * @property {String} docTypes.act.name The name of the act doctype
     * @property {Object} docTypes.bill Properties of the bill doctype of AkomaNtoso
     * @property {String} docTypes.bill.name The name of the bill doctype
     * @property {Object} docTypes.debateReport Properties of the debateReport doctype of AkomaNtoso
     * @property {String} docTypes.debateReport.name The name of the debateReport doctype
     * @property {Object} docTypes.debate Properties of the debate doctype of AkomaNtoso
     * @property {String} docTypes.debate.name The name of the debate doctype
     * @property {Object} docTypes.statement Properties of the statement doctype of AkomaNtoso
     * @property {String} docTypes.statement.name The name of the statement doctype
     * @property {Object} docTypes.amendment Properties of the amendment doctype of AkomaNtoso
     * @property {String} docTypes.amendment.name The name of the amendment doctype
     * @property {Object} docTypes.judgment Properties of the judgment doctype of AkomaNtoso
     * @property {String} docTypes.judgment.name The name of the judgment doctype
     * @property {Object} docTypes.portion Properties of the portion doctype of AkomaNtoso
     * @property {String} docTypes.portion.name The name of the portion doctype
     * @property {Object} docTypes.doc Properties of the doc doctype of AkomaNtoso
     * @property {String} docTypes.doc.name The name of the doc doctype
     * @property {Object} meta Properties of AkomaNtoso meta element
     * @property {String} meta.name The name of the AkomaNtoso meta element
     * @example
     * {
     *  "akomaNtosoRoot": "akomaNtoso",
     *  "docTypes": {
     *     "amendmentList": {
     *        "name": "amendmentList"
     *     },
     *     "officialGazette": {
     *        "name": "officialGazette"
     *     },
     *     "documentCollection": {
     *        "name": "documentCollection"
     *     },
     *     "act": {
     *        "name": "act"
     *     },
     *     "bill": {
     *        "name": "bill"
     *     },
     *     "debateReport": {
     *        "name": "debateReport"
     *     },
     *     "debate": {
     *        "name": "debate"
     *     },
     *     "statement": {
     *        "name": "statement"
     *     },
     *     "amendment": {
     *        "name": "amendment"
     *     },
     *     "judgment": {
     *        "name": "judgment"
     *     },
     *     "portion": {
     *        "name": "portion"
     *     },
     *     "doc": {
     *        "name": "doc"
     *     }
     *  },
     *  "meta": {
     *     "name": "meta"
     *  }
     *}
   */
   /**
     * Object schema for the akomando hierarchies identifier
     * @typedef {Object} akomando.hier.identifier
     * @property {String} name The name of the hierarchy
     * @property {String} nameAttribute The name of the hierarchy according to its name attribute
     * @property {String} eId The expression id of the hierarchy
     * @property {String} wId The work id of the hierarchy
     * @property {String} GUID The guid of the hierachy
     * @property {String} xpath The xpath of the hierarchy starting by the main container element
     * @property {Number} level The level of the hierarchy in respect of the main hierarchy
     *
     * @example
     * {
     *   "name": "hcontainer",
     *   "nameAttribute": "schedule",
     *   "eId": "schedule",
     *   "wId": "",
     *   "GUID": "",
     *   "xpath": "akomaNtoso[1]//body[1]/hcontainer[1]/hcontainer[1]",
     *   "level": 2
     * }
     *
   */
   /**
     * Object schema for the akomando identifiers
     * @typedef {Object} akomando.identifier
     * @property {String} name The name of the element
     * @property {String} eId The expression id of the element
     * @property {String} wId The work id of the element
     * @property {String} GUID The guid of the element
     *
     * @example
     * {
     *   "name": "preface",
     *   "eId": "preface",
     *   "wId": "",
     *   "GUID": "",
     * }
     *
     * @example
     * {
     *   "name": "mod",
     *   "eId": "art_2__para_1__list_1__point_a__mod_2",
     *   "wId": "art_2__para_1__point_a__content_1__mod_1",
     *   "GUID": "",
     * }
   */
   /**
     * Object schema for the akomando hierarchies identifiers
     * @typedef {Object} akomando.text.identifier
     * @property {String} name The name of the element
     * @property {String} text The text of the element
     *
     * @example
     * {
     *   "name": "preface",
     *   "text": "This is the preface of the Akoma Ntoso document",
     *   "xpath": "akomaNtoso[1]/preface[1]",
     * }
     *
     * @example
     * {
     *   "name": "article",
     *   "text": "Article 1 of law 17 of 2017 must be changed as",
     *   "xpath": "akomaNtoso[1]/body[1]/article[1]",
     * }
   */
   /**
     * Object schema for the akomando elements count
     * @typedef {Object} akomando.elements.count
     * @property {Number} total The total number of elements
     * @property {Object} elements The array of all elements contained in the document
     * @property {String} elements.name The name of the element
     * @property {Number} elements.count The number of elements having the given name
     *
     * @example
     * {
     *   "total": 1697,
     *   "elements":[
     *      { "name": "FRBRExpression", count: 1 },
     *      { "name": "FRBRManifestation", count: 1 },
     *      { "name": "FRBRWork", count: 1 },
     *      { "name": "FRBRalias", count: 2 },
     *      { "name": "FRBRauthor", count: 3 },
     *      { "name": "FRBRcountry", count: 1 },
     *      { "name": "FRBRdate", count: 3 },
     *      { "name": "FRBRlanguage", count: 1 },
     *      { "name": "FRBRthis", count: 3 },
     *      { "name": "FRBRuri", count: 3 },
     *      { "name": "TLCConcept", count: 2 },
     *      { "name": "TLCOrganization", count: 4 },
     *      { "name": "TLCReference", count: 74 },
     *      { "name": "act", count: 1 },
     *      { "name": "activeModifications", count: 1 },
     *      { "name": "activeRef", count: 9 },
     *      { "name": "akomaNtoso", count: 1 },
     *      { "name": "analysis", count: 1 },
     *      { "name": "article", count: 10 },
     *      { "name": "authorialNote", count: 1 },
     *      { "name": "body", count: 1 },
     *      { "name": "classification", count: 1 },
     *      { "name": "conclusions", count: 1 },
     *      { "name": "container", count: 2 },
     *      { "name": "content", count: 58 },
     *      { "name": "date", count: 1 },
     *      { "name": "destination", count: 26 },
     *      { "name": "docDate", count: 1 },
     *      { "name": "docNumber", count: 1 },
     *      { "name": "docTitle", count: 1 },
     *      { "name": "docType", count: 1 },
     *      { "name": "eol", count: 196 },
     *      { "name": "eventRef", count: 1 },
     *      { "name": "hasAttachment", count: 1 },
     *      { "name": "heading", count: 10 },
     *      { "name": "identification", count: 1 },
     *      { "name": "intro", count: 11 },
     *      { "name": "keyword", count: 3 },
     *      { "name": "lifecycle", count: 1 },
     *      { "name": "list", count: 11 },
     *      { "name": "meta", count: 1 },
     *      { "name": "mod", count: 27 },
     *      { "name": "mref", count: 2 },
     *      { "name": "new", count: 25 },
     *      { "name": "note", count: 9 },
     *      { "name": "noteRef", count: 9 },
     *      { "name": "notes", count: 1 },
     *      { "name": "num", count: 78 },
     *      { "name": "old", count: 20 },
     *      { "name": "original", count: 1 },
     *      { "name": "p", count: 148 },
     *      { "name": "paragraph", count: 39 },
     *      { "name": "point", count: 30 },
     *      { "name": "preamble", count: 1 },
     *      { "name": "preface", count: 1 },
     *      { "name": "proprietary", count: 1 },
     *      { "name": "publication", count: 1 },
     *      { "name": "quotedStructure", count: 46 },
     *      { "name": "ref", count: 97 },
     *      { "name": "references", count: 1 },
     *      { "name": "signature", count: 5 },
     *      { "name": "source", count: 26 },
     *      { "name": "text", count: 650 },
     *      { "name": "textualMod", count: 26 }
     *   ]
     * }
     *
     * @example
     * {
     *   "total": 97,
     *   "elements": [
     *      {
     *         "name": 'ref',
     *         "count": 97
     *      }
     *   ]
     * }
   */

   /**
    * Object schema for the functions _getElementAttributes and _getElementsAttributes
    * @typedef {Object} akomando.elements.attributes
    * @property {String} elementName the name of the analysed element
    * @property {Array} attributeList the list of the element's attributes
    *
    * @example
    * [
    *  {
    *     elementName: "FRBRthis"
    *     attributes: [
    *        {
    *           name: "value"
    *           value: "/akn/it/act/decretolegislativo/stato/2016-01-15/decreto-legislativo/!main"
    *        }
    *      ]
    *   },
    *  {
    *     elementName: "FRBRalias"
    *     attributes: [
    *        {
    *           name: "value"
    *           value: "urn:nir:stato:decreto.legislativo:2016-01-15;8"
    *        },
    *        {
    *           name: "name"
    *           value: ""urn:nir"
    *        }
    *      ]
    *   }
    * ]
   */

   /**
    * Returns an akomando Object
    * @protected
    * @throws {Error} Throws error when the aknString is null
    * @throws {Error} Throws error when the aknString is not a String
    * @param {Object} options Options for building the akomando
    * @param {String} options.aknString The AkomaNtoso string
    * @param {Object} [options.config] The configuration of the akomando according to {@link akomando.config} specications.
    * If config is not send then the ones in config/config.json are used.
    * @return {Object} An {@link _Akomando} object
   */
   constructor({
      aknString,
      config = aknConfig,
   }={}){
      if(typeCheck('Undefined',aknString)){
         throw new Error(errors._AKNStringNotSpecified);
      }
      if(!typeCheck('String',aknString)){
         throw new Error(errors._InputIsNotAString);
      }
      /**
       * @property {Object} _akn Stores the {@link _DocumentHandler} object
      */
      this._akn = new _DocumentHandler({
         config,
      });
      this._akn.parseAKNString(aknString);
      /**
       * @property {Object} _config Stores the {@link akomando.config} object
      */
      this._config = config;
   }

   /**
    * Return the AkomaNtoso document type of the loaded document
    * @protected
    * @throws {Error} Throws error when the AkomaNtoso document is not set
    * @example <caption>NODE (ES6)</caption>
    * // Logs the doc type of the loaded AkomaNtoso document.
    * import { createAkomando } from 'akomando';
    *
    * const myAkomaNtosoStream = fs.readFileSync('./my-akomantoso.xml');
    * const myAkomaNtosoString = myAkomaNtosoStream.toString();
    * const myAkomando = createAkomando({
    *    aknString: myAkomaNtosoString,
    * });
    * console.log(myAkomando.getDocType());
    * @example <caption>BROWSER</caption>
    * // Logs the doc type of the loaded AkomaNtoso document.
    * <html>
    *    <head>
    *       <title>akomando example getDocType</title>
    *    </head>
    *    <body>
    *       <script src="https://code.jquery.com/jquery-3.1.1.min.js"
    *               integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
    *               crossorigin="anonymous"></script>
    *       <script src="akomando.min.js" charset="UTF-8"></script>
    *       <script>
    *          // load the content of an AkomaNtoso file in some way. Here I use jQuery.
    *          var myAkomaNtosoString = $.ajax({
    *             url: 'my-akomantoso.xml',
    *             async: false
    *          }).responseText;

    *          var myAkomando =  akomando.createAkomando({
    *             aknString: myAkomaNtosoString,
    *          });
    *          console.log(myAkomando.getDocType());
    *       </script>
    *    </body>
    * </html>
    * @example <caption>COMMAND LINE (linux)</caption>
    * // returns the doc type of the AkomaNtoso file having path "path/to/file"
    * $ akomando get-doc-type <path/to/file>
    * @example <caption>COMMAND LINE (MS-DOS)</caption>
    * // returns the doc type of the AkomaNtoso file having path "path\to\file"
    * akomando get-doc-type path\to\file
    * @return {String} the document type of the loaded AkomaNtoso
   */
   getDocType(){
      return this._akn.getDocType();
   }

   /**
    * Return the AkomaNtoso document in the requested serialization
    * @protected
    * @throws {Error} Throws error when the AkomaNtoso document is not set
    * @throws {Error} Throws error when serialize parameter is not a String
    * @throws {Error} Throws error when the requested serialization is not supported
    * @throws {Error} Throws error when addHtmlElement parameter is not a Boolean
    * @throws {Error} Throws error when addHeadElement parameter is not a Boolean
    * @throws {Error} Throws error when addBodyElement parameter is not a Boolean
    * @example <caption>NODE (ES6)</caption>
    * import { createAkomando } from 'akomando';
    *
    * const myAkomaNtosoStream = fs.readFileSync('./my-akomantoso.xml');
    * const myAkomaNtosoString = myAkomaNtosoStream.toString();
    *
    * const myAkomando = createAkomando({
    *    aknString: myAkomaNtosoString,
    * });
    *
    * // logs the loaded AkomaNtoso as XML DOM
    * console.log(myAkomando.getAkomaNtoso());
    *
    * // logs the loaded AkomaNtoso as XLM String
    * console.log(myAkomando.getAkomaNtoso({
    *    serialize: 'AKNString'
    * });
    *
    * // logs the loaded AkomaNtoso as JSON
    * console.log(myAkomando.getAkomaNtoso({
    *    serialize: 'JSON'
    * }));
    *
    * // logs the loaded AkomaNtoso as JSON String
    * console.log(myAkomando.getAkomaNtoso({
    *    serialize: 'JSONString'
    * }));
    *
    * // logs the loaded AkomaNtoso as HTML DOM
    * console.log(myAkomando.getAkomaNtoso({
    *    serialize: 'HTMLDom'
    * }));
    *
    * // logs the loaded AkomaNtoso as HTML String, without inserting the head element and the body element
    * console.log(myAkomando.getAkomaNtoso({
    *    serialize: 'HTMLString',
    *    addHeadElement: false,
    *    addBodyElement: false,
    * }));
    * @example <caption>BROWSER</caption>
    * <html>
    *    <head>
    *       <title>akomando example getAkomaNtoso</title>
    *    </head>
    *    <body>
    *       <script src="https://code.jquery.com/jquery-3.1.1.min.js"
    *               integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
    *               crossorigin="anonymous"></script>
    *       <script src="akomando.min.js" charset="UTF-8"></script>
    *       <script>
    *          // load the content of an AkomaNtoso file in some way. Here I use jQuery.
    *          var myAkomaNtosoString = $.ajax({
    *             url: 'my-akomantoso.xml',
    *             async: false
    *          }).responseText;
    *
    *          var myAkomando =  akomando.createAkomando({
    *             aknString: myAkomaNtosoString,
    *          });
    *
    *          // logs the loaded AkomaNtoso as XML DOM
    *          console.log(myAkomando.getAkomaNtoso());
    *
    *          // logs the loaded AkomaNtoso as XML String
    *          console.log(myAkomando.getAkomaNtoso({
    *             serialize: 'AKNString'
    *          });
    *
    *          // logs the loaded AkomaNtoso as JSON
    *          console.log(myAkomando.getAkomaNtoso({
    *             serialize: 'JSON'
    *          }));
    *
    *          // logs the loaded AkomaNtoso as JSON String
    *          console.log(myAkomando.getAkomaNtoso({
    *             serialize: 'JSONString'
    *          }));
    *
    *          // logs the loaded AkomaNtoso as HTML DOM
    *          console.log(myAkomando.getAkomaNtoso({
    *             serialize: 'HTMLDom'
    *          }));
    *
    *          // logs the loaded AkomaNtoso as HTML String, without inserting the head element and the body element
    *          console.log(myAkomando.getAkomaNtoso({
    *             serialize: 'HTMLString'
    *             addHeadElement: false,
    *             addBodyElement: false,
    *          }));
    *       </script>
    *    </body>
    *</html>
    * @example <caption>COMMAND LINE (linux)</caption>
    * // returns the AkomaNtoso file having path "path/to/file" as an AkomaNtoso string
    * $ akomando get-akomantoso <path/to/file>
    *
    * // returns the AkomaNtoso file having path "path/to/file" as a JSON string
    * $ akomando get-akomantoso <path/to/file> -s json
    *
    * // returns the AkomaNtoso file having path "path/to/file" as a HTML string without head and body elements
    * $ akomando get-akomantoso <path/to/file> -s html --no-head --no-body
    * @example <caption>COMMAND LINE (MS-DOS)</caption>
    * // returns the AkomaNtoso file having path "path\to\file" as an AkomaNtoso string
    * akomando get-akomantoso path\to\file
    *
    * // returns the AkomaNtoso file having path "path\to\file" as a JSON string
    * akomando get-akomantoso path\to\file -s json
    *
    * // returns the AkomaNtoso file having path "path\to\file" as a HTML string without head and body elements
    * akomando get-akomantoso path\to\file -s html --no-head --no-body

    * @param {Object} [options] Options for the retrieved document
    * @param {String} [options.serialize=AKNDom] The serialization of the returning document. Possible values are:
    * - AKNDom - returns the XML <a target="_blank" href="https://www.w3.org/TR/2000/REC-DOM-Level-2-Core-20001113/core.html#i-Document">
    *   Document</a> serialization of the Akomantoso
    * - AKNString - returns the String serialization of the Akomantoso Document
    * - HTMLDom - returns the HTML <a target="_blank" href="https://www.w3.org/TR/2000/REC-DOM-Level-2-Core-20001113/core.html#i-Document">
    *   Document</a> serialization of the Akomantoso
    * - HTMLString - returns the HTML string serialization of the Akomantoso
    * - JSON - returns the <a target="_blank" href="https://developer.mozilla.org/it/docs/Web/JavaScript/Reference/Global_Objects/JSON">
    *   JSON</a> serialization of the Akomantoso
    * - JSONString - returns the JSON string serialization of the Akomantoso
    * - TEXT - returns text of all document
    * - TEXTBody - returns text of only main container element
    * @param {Boolean} [options.addHtmlElement=true] - Set it to true to add the html element into the created document
    * @param {Boolean} [options.addHeadElement=true] - Set it to true to add the head element into the created document
    * @param {Boolean} [options.addBodyElement=true] - Set it to true to add the body element into the created document
    * @param {Boolean} [options.newLines=true] - Set it to true to mantain the newLines in the text
    * @return {Object} The AkomaNtoso document in the requested serialization
   */
   getAkomaNtoso({
      serialize = 'AKNDom',
      addHtmlElement,
      addHeadElement,
      addBodyElement,
      newLines,
   } = {}){
      const results = this._akn.getAKNDocument({
         serialize,
         addHtmlElement,
         addHeadElement,
         addBodyElement,
         newLines,
      });
      return results;
   }


   /**
    * Get the documents metadata in the requested serialization
    * @protected
    * @throws {Error} Throws error when the AkomaNtoso document is not set
    * @throws {Error} Throws error when serialize parameter is not a String
    * @throws {Error} Throws error when metaRoot parameter is not a String
    * @throws {Error} Throws error when the requested serialization is not supported
    * @throws {Error} Throws error when addHtmlElement parameter is not a Boolean
    * @throws {Error} Throws error when addHeadElement parameter is not a Boolean
    * @throws {Error} Throws error when addBodyElement parameter is not a Boolean
    * @example <caption>NODE (ES6)</caption>
    * import { createAkomando } from 'akomando';
    *
    * const myAkomaNtosoStream = fs.readFileSync('./my-akomantoso.xml');
    * const myAkomaNtosoString = myAkomaNtosoStream.toString();
    *
    * const myAkomando = createAkomando({
    *    aknString: myAkomaNtosoString,
    * });
    *
    * // logs all the meta of the loaded AkomaNtoso as an AkomaNtoso XML Document
    * console.log(myAkomando.getMeta());
    *
    * // logs all the meta of the loaded AkomaNtoso as an AkomaNtoso string
    * console.log(myAkomando.getMeta({
    *    serialize: 'AKNString'
    * });
    *
    * // logs all the meta of the loaded AkomaNtoso as a JSON object
    * console.log(myAkomando.getMeta({
    *    serialize: 'JSON'
    * }));
    *
    * // logs all the meta of the loaded AkomaNtoso as a JSON string
    * console.log(myAkomando.getMeta({
    *    serialize: 'JSONString'
    * }));
    *
    * // logs all the meta of the loaded AkomaNtoso as a HTML Document
    * console.log(myAkomando.getMeta({
    *    serialize: 'HTMLDom'
    * }));
    *
    * // logs all the meta of the loaded AkomaNtoso as a HTML string
    * console.log(myAkomando.getMeta({
    *    serialize: 'HTMLString'
    * }));
    *
    *
    * // logs all meta FRBRcountry of the loaded AkomaNtoso as a JSON object
    * console.log(myAkomando.getMeta({
    *    serialize: 'JSON',
    *    metaRoot: 'FRBRthis',
    * }));
    * @example <caption>BROWSER</caption>
    * <html>
    *    <head>
    *       <title>akomando example getMeta</title>
    *    </head>
    *    <body>
    *       <script src="https://code.jquery.com/jquery-3.1.1.min.js"
    *               integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
    *               crossorigin="anonymous"></script>
    *       <script src="akomando.min.js" charset="UTF-8"></script>
    *       <script>
    *          // load the content of an AkomaNtoso file in some way. Here I use jQuery.
    *          var myAkomaNtosoString = $.ajax({
    *             url: 'my-akomantoso.xml',
    *             async: false
    *          }).responseText;
    *
    *          var myAkomando =  akomando.createAkomando({
    *             aknString: myAkomaNtosoString,
    *          });
    *
    *          // logs all the meta of the loaded AkomaNtoso as an AkomaNtoso XML Document
    *          console.log(myAkomando.getMeta());
    *
    *          // logs all the meta of the loaded AkomaNtoso as an AkomaNtoso XML string
    *          console.log(myAkomando.getMeta({
    *             serialize: 'AKNString'
    *          });
    *
    *          // logs all the meta of the loaded AkomaNtoso as a JSON object
    *          console.log(myAkomando.getMeta({
    *             serialize: 'JSON'
    *          }));
    *
    *          // logs all the meta of the loaded AkomaNtoso as a JSON string
    *          console.log(myAkomando.getMeta({
    *             serialize: 'JSONString'
    *          }));
    *
    *          // logs all the meta of the loaded AkomaNtoso as a HTML Document
    *          console.log(myAkomando.getMeta({
    *             serialize: 'HTMLDom'
    *          }));
    *
    *          // logs all the meta of the loaded AkomaNtoso as a HTML string
    *          console.log(myAkomando.getMeta({
    *             serialize: 'HTMLString'
    *          }));
    *
    *          // logs all meta FRBRcountry of the loaded AkomaNtoso as a JSON object
    *          console.log(myAkomando.getMeta({
    *             serialize: 'JSON',
    *             metaRoot: 'FRBRthis',
    *          }));
    *       </script>
    *    </body>
    *</html>
    * @example <caption>COMMAND LINE (linux)</caption>
    * // returns the meta of the AkomaNtoso file having path "path/to/file" as an AkomaNtoso string
    * $ akomando get-meta <path/to/file>
    *
    * // returns the meta of the AkomaNtoso file having path "path/to/file" as a JSON string
    * $ akomando get-meta <path/to/file> -s json
    *
    * // returns all meta FRBRcountry of the AkomaNtoso file having path "path/to/file" as a JSON string
    * $ akomando get-meta <path/to/file> -s json -m FRBRcountry
    * @example <caption>COMMAND LINE (MS-DOS)</caption>
    * // returns the meta of the AkomaNtoso file having path "path\to\file" as an AkomaNtoso string
    * akomando get-meta path\to\file
    *
    * // returns the meta of the AkomaNtoso file having path "path\to\file" as a JSON string
    * akomando get-meta path\to\file -s json
    *
    * // returns all meta FRBRcountry of the AkomaNtoso file having path "path\to\file" as a JSON string
    * akomando get-meta path\to\file -s json -m FRBRcountry
    * @throws {Error} Throws error when the AkomaNtoso document is not set
    * @throws {Error} Throws error when the serialization is not supported
    * @param {Object} [options] Options for the requested meta
    * @param {String} [options.serialize=AKNDom] The serialization of the returning meta. Possible values are:
    * - AKNDom - returns the xml <a target="_blank" href="https://www.w3.org/TR/2000/REC-DOM-Level-2-Core-20001113/core.html#i-Document">
    *     Document</a> serialization of requested meta
    * - AKNString - returns the String serialization of requested meta
    * - HTMLDom - returns the HTML <a target="_blank" href="https://www.w3.org/TR/2000/REC-DOM-Level-2-Core-20001113/core.html#i-Document">
    *     Document</a> serialization of requested meta
    * - HTMLString - returns the HTML string serialization of requested meta
    * - JSON - returns the <a target="_blank" href="https://developer.mozilla.org/it/docs/Web/JavaScript/Reference/Global_Objects/JSON">
    *     JSON</a> serialization of requested meta
    * - JSONString - returns the JSON string serialization of requested meta
    *
    * @param {String} [options.metaRoot] The name of meta that must be retrieved. It can be qualified or not qualified.
    * @param {String} [options.insertDataXPath=false] - Set it to true to add the data-akomando-xpath atribute to each of the returned elements
    * @param {String} [options.addHtmlElement=true] - Set it to true to add the html element into the created document (if the HTML serialization is required)
    * @param {String} [options.addHeadElement=true] - Set it to true to add the head element into the created document (if the HTML serialization is required)
    * @param {String} [options.addBodyElement=true] - Set it to true to add the body element into the created document (if the HTML serialization is required)
    * @return {Array} An array containing all founded meta in the requested serialization
   */
   getMeta({
      serialize = 'AKNDom',
      metaRoot,
      insertDataXPath,
      addHtmlElement,
      addHeadElement,
      addBodyElement,
   } = {}){
      const results = this._akn.getMeta({
         serialize,
         metaRoot,
         insertDataXPath,
         addHtmlElement,
         addHeadElement,
         addBodyElement,
      });
      return results;
   }

   /**
    * Returns the components contained in the document in an object structured as {@link akomando.components.getComponents}
    * @protected
    * @throws {Error} Throws error when the AkomaNtoso document is not set
    * @throws {Error} Throws error when serialize parameter is not a String
    * @throws {Error} Throws error when the requested serialization is not supported
    * @throws {Error} Throws error when detailedInfo parameter is not a Boolean
    * @throws {Error} Throws error when insertDataXPath parameter is not a Boolean
    * @throws {Error} Throws error when addHtmlElement parameter is not a Boolean (if the HTML serialization is required)
    * @throws {Error} Throws error when addHeadElement parameter is not a Boolean (if the HTML serialization is required)
    * @throws {Error} Throws error when addBodyElement parameter is not a Boolean (if the HTML serialization is required)
    * @example <caption>NODE (ES6)</caption>
    * import { createAkomando } from 'akomando';
    *
    * const myAkomaNtosoStream = fs.readFileSync('./my-akomantoso.xml');
    * const myAkomaNtosoString = myAkomaNtosoStream.toString();
    *
    * const myAkomando = createAkomando({
    *    aknString: myAkomaNtosoString,
    * });
    *
    * // logs an object containing all the components contained in
    * // the loaded AkomaNtoso as AkomaNtoso XML Documents
    * console.log(myAkomando.getComponents());
    *
    * // logs an object containing all the components contained in
    * // the loaded AkomaNtoso as AkomaNtoso XML Strings
    * console.log(myAkomando.getComponents({
    *    serialize: 'AKNString'
    * });
    *
    * // logs an object containing all the components contained in
    * // the loaded AkomaNtoso as JSON Objects
    * console.log(myAkomando.getComponents({
    *    serialize: 'JSON'
    * }));
    *
    * // logs an object containing all the components contained in
    * // the loaded AkomaNtoso as JSON Strings
    * console.log(myAkomando.getComponents({
    *    serialize: 'JSONString'
    * }));
    *
    * // logs an object containing all the components contained in
    * // the loaded AkomaNtoso as HTML Documents
    * console.log(myAkomando.getComponents({
    *    serialize: 'HTMLDom'
    * }));
    *
    * // logs an object containing all the components contained in
    * // the loaded AkomaNtoso as HTML Strings. It includes the html element
    * // and the body element to each HTML
    * console.log(myAkomando.getComponents({
    *    serialize: 'HTMLString',
    *    addHtmlElement: true,
    *    addBodyElement: true,
    * }));
    *
    * @example <caption>BROWSER</caption>
    * <html>
    *    <head>
    *       <title>akomando example getMeta</title>
    *    </head>
    *    <body>
    *       <script src="https://code.jquery.com/jquery-3.1.1.min.js"
    *               integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
    *               crossorigin="anonymous"></script>
    *       <script src="akomando.min.js" charset="UTF-8"></script>
    *       <script>
    *          // load the content of an AkomaNtoso file in some way. Here I use jQuery.
    *          var myAkomaNtosoString = $.ajax({
    *             url: 'my-akomantoso.xml',
    *             async: false
    *          }).responseText;
    *
    *          var myAkomando =  akomando.createAkomando({
    *             aknString: myAkomaNtosoString,
    *          });
    *
    *          // logs an object containing all the components contained in
    *          // the loaded AkomaNtoso as AkomaNtoso XML Documents
    *          console.log(myAkomando.getComponents());
    *
    *          // logs an object containing all the components contained in
    *          // the loaded AkomaNtoso as AkomaNtoso XML Strings
    *          // logs all the meta of the loaded AkomaNtoso as an AkomaNtoso XML string
    *          console.log(myAkomando.getComponents({
    *             serialize: 'AKNString'
    *          });
    *
    *          // logs an object containing all the components contained in
    *          // the loaded AkomaNtoso as JSON objects
    *          console.log(myAkomando.getComponents({
    *             serialize: 'JSON'
    *          }));
    *
    *          // logs an object containing all the components contained in
    *          // the loaded AkomaNtoso as JSON strings
    *          console.log(myAkomando.getComponents({
    *             serialize: 'JSONString'
    *          }));
    *
    *          // logs an object containing all the components contained in
    *          // the loaded AkomaNtoso as HTML documents
    *          console.log(myAkomando.getComponents({
    *             serialize: 'HTMLDom'
    *          }));
    *
    *          // logs an object containing all the components contained in
    *          // the loaded AkomaNtoso as HTML Strings. It includes the html element
    *          // and the body element to each HTML
    *          console.log(myAkomando.getComponents({
    *             serialize: 'HTMLString'
    *             serialize: 'HTMLString',
    *             addHtmlElement: true,
    *             addBodyElement: true,
    *          }));
    *
    *       </script>
    *    </body>
    *</html>
    * @example <caption>COMMAND LINE (linux)</caption>
    * // logs the string representation of an object containing
    * // all the components contained in the loaded AkomaNtoso as AkomaNtoso XML Documents
    * $ akomando get-components <path/to/file>
    *
    * // logs the string representation of an object containing
    * // all the components contained in the loaded AkomaNtoso as JSON Objects
    * $ akomando get-components <path/to/file> -s json
    *
    * // logs the string representation of an object containing
    * // all the components contained in the loaded AkomaNtoso as HTML Documents
    * // without includung the data-akomando-xpath attibute to the elements
    * // contained in the HTML
    * $ akomando get-components <path/to/file> -s html --no-data--xpath
    * @example <caption>COMMAND LINE (MS-DOS)</caption>
    * // logs the string representation of an object containing
    * // all the components contained in the loaded AkomaNtoso as AkomaNtoso XML Documents
    * akomando get-components path\to\file
    *
    * // logs the string representation of an object containing
    * // all the components contained in the loaded AkomaNtoso as JSON Objects
    * akomando get-components path\to\file -s json
    *
    * // logs the string representation of an object containing
    * // all the components contained in the loaded AkomaNtoso as HTML Documents
    * // without includung the data-akomando-xpath attibute to the elements
    * // contained in the HTML
    * akomando get-components path\to\file -s html --no-data--xpath
    * @param {Object} [options] Options for the requested meta
    * @param {String} [options.serialize=AKNDom] The serialization of the returning meta. Possible values are:
     * - AKNDom - returns the XML <a target="_blank" href="https://www.w3.org/TR/2000/REC-DOM-Level-2-Core-20001113/core.html#i-Document">
     *     Document</a> serialization of the components contained in the document
     * - AKNString - returns the String serialization of the components contained in the document
     * - HTMLDom - returns the HTML <a target="_blank" href="https://www.w3.org/TR/2000/REC-DOM-Level-2-Core-20001113/core.html#i-Document">
     *     Document</a> serialization of the components contained in the document
     * - HTMLString - returns the HTML string serialization of the components contained in the document
     * - JSON - returns the <a target="_blank" href="https://developer.mozilla.org/it/docs/Web/JavaScript/Reference/Global_Objects/JSON">
     *     JSON</a> serialization of the components contained in the document
     * - JSONString - returns the JSON string serialization of the components contained in the document
    *
    * @param {String} [options.detailedInfo=true] - If it is set to false, the function returns only an array containing the components in the requested serialization
    * @param {Boolean} [options.insertDataXPath=false] - Set it to true to add the data-akomando-xpath atribute to each of the returned elements
    * @param {String} [options.addHtmlElement=true] - Set it to true to add the html element into the created documents (if the HTML serialization is required)
    * @param {String} [options.addHeadElement=true] - Set it to true to add the head element into the created document (if the HTML serialization is required)
    * @param {String} [options.addBodyElement=true] - Set it to true to add the body element into the created document (if the HTML serialization is required)
    * @return {Array} An array that contains the information about the components contained in the document. The array contains an object structured as {@link akomando.components.getComponents}
   */
   getComponents({
      serialize = 'AKNDom',
      detailedInfo = true,
      insertDataXPath,
      addHtmlElement,
      addHeadElement,
      addBodyElement,
   } = {}){
      const results = this._akn.getComponents({
         serialize,
         detailedInfo,
         insertDataXPath,
         addHtmlElement,
         addHeadElement,
         addBodyElement,
      });
      return results;
   }

   /**
    * Returns all ids of all hierarchies of the loaded AkomaNtoso document
    * @protected
    * @example <caption>NODE (ES6)</caption>
    * import { createAkomando } from 'akomando';
    *
    * const myAkomaNtosoStream = fs.readFileSync('./my-akomantoso.xml');
    * const myAkomaNtosoString = myAkomaNtosoStream.toString();
    *
    * const myAkomando = createAkomando({
    *    aknString: myAkomaNtosoString,
    * });
    *
    * // logs all hierarchies identifiers of the loaded AkomaNtoso sorting them by their position in document
    * console.log(myAkomando.getHierIdentifiers());
    *
    * // logs all hierarchies identifiers of the loaded AkomaNtoso sorting them by their xpath
    * console.log(myAkomando.getHierIdentifiers({
    *    sortBy: 'xpath'
    * }));
    *
    * // logs all the meta of the loaded AkomaNtoso ordering them descending
    * console.log(myAkomando.getHierIdentifiers({
    *    order: 'descending'
    * });
    *
    * // logs all hierarchies identifiers of articles of the loaded AkomaNtoso
    * console.log(myAkomando.getHierIdentifiers({
    *    filterByName: 'articles'
    * }));
    *
    * // logs all hierarchies identifiers of hierarchies having content of the loaded AkomaNtoso
    * console.log(myAkomando.getHierIdentifiers({
    *    filterByContent: true
    * }));
    *
    * @example <caption>BROWSER</caption>
    * <html>
    *    <head>
    *       <title>akomando example getMeta</title>
    *    </head>
    *    <body>
    *       <script src="https://code.jquery.com/jquery-3.1.1.min.js"
    *               integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
    *               crossorigin="anonymous"></script>
    *       <script src="akomando.min.js" charset="UTF-8"></script>
    *       <script>
    *          // load the content of an AkomaNtoso file in some way. Here I use jQuery.
    *          var myAkomaNtosoString = $.ajax({
    *             url: 'my-akomantoso.xml',
    *             async: false
    *          }).responseText;
    *
    *          var myAkomando =  akomando.createAkomando({
    *             aknString: myAkomaNtosoString,
    *          });
    *
    *          // logs all hierarchies identifiers of the loaded AkomaNtoso sorting them by their position in document
    *          console.log(myAkomando.getHierIdentifiers());
    *
    *          // logs all hierarchies identifiers of the loaded AkomaNtoso sorting them by their xpath
    *          console.log(myAkomando.getHierIdentifiers({
    *             sortBy: 'xpath'
    *          });
    *
    *          // logs all the meta of the loaded AkomaNtoso ordering them descending
    *          console.log(myAkomando.getHierIdentifiers({
    *             order: 'descending'
    *          }));
    *
    *          // logs all hierarchies identifiers of articles of the loaded AkomaNtoso
    *          console.log(myAkomando.getHierIdentifiers({
    *             filterByName: 'article'
    *          }));
    *
    *          // logs all hierarchies identifiers of hierarchies having content of the loaded AkomaNtoso
    *          console.log(myAkomando.getHierIdentifiers({
    *             filterByContent: true
    *          }));
    *       </script>
    *    </body>
    *</html>
    * @example <caption>COMMAND LINE (linux)</caption>
    * // returns all hierarchies identifiers of the AkomaNtoso file having path "path/to/file" sorting them by their position in document
    * $ akomando get-hier-identifiers <path/to/file>
    *
    * // return logs all hierarchies identifiers of the AkomaNtoso file having path "path/to/file" sorting them by their xpath
    * $ akomando get-hier-identifiers <path/to/file> --sort-by xpath
    *
    * // returns all the meta of the AkomaNtoso file having path "path/to/file" ordering them descending
    * $ akomando get-hier-identifiers <path/to/file> --order descending
    *
    * // returns all hierarchies identifiers of articles of the AkomaNtoso file having path "path/to/file"
    * $ akomando get-hier-identifiers <path/to/file> --filter-by-name article
    *
    * // returns all hierarchies identifiers of hierarchis having content of the AkomaNtoso file having path "path/to/file"
    * $ akomando get-hier-identifiers <path/to/file> --filter-by-content
    * @example <caption>COMMAND LINE (MS-DOS)</caption>
    * // returns all hierarchies identifiers of the AkomaNtoso file having path "path\to\file" sorting them by their position in document
    * $ akomando get-hier-identifiers path\to\file
    *
    * // return logs all hierarchies identifiers of the AkomaNtoso file having path "path\to\file" sorting them by their xpath
    * $ akomando get-hier-identifiers path\to\file --sort-by xpath
    *
    * // returns all the meta of the AkomaNtoso file having path "path\to\file" ordering them descending
    * $ akomando get-hier-identifiers path\to\file --order descending
    *
    * // returns all hierarchies identifiers of articles of the AkomaNtoso file having path "path\to\file"
    * $ akomando get-hier-identifiers path\to\file --filter-by-name article
    *
    * // returns all hierarchies identifiers of hierarchis having content of the AkomaNtoso file having path "path\to\file"
    * $ akomando get-hier-identifiers path\to\file --filter-by-content
    * @throws {Error} Throws error when the AkomaNtoso document is not set
    * @throws {Error} Throws error when sortBy parameter is not a String
    * @throws {Error} Throws error when the requested sorting is not supported
    * @throws {Error} Throws error when order parameter is not a String
    * @throws {Error} Throws error when the requested ordering is not supported
    * @throws {Error} Throws error when filterByName parameter is not a String
    * @throws {Error} Throws error when filterByContent parameter is not a Boolean
    * @param {Object} [options] The options for resulting {@link akomando.hier.identifiers} object
    * @param {String} [options.sortyBy=position] The field on wich results must be sorted. Possible values are:
    *
    * - position - The position of the element insiede the document
    * - name - The name of the element
    * - eId - The eId of the element
    * - wId - The wId of the element
    * - GUID - The guid of the element
    * - xpath - The xpath of the element
    * - level - The level of the element
    *
    * @param {String} [options.order=ascending] The ordering of the returnet elements. Possible values are:
    *
    * - ascending - Results are ordered in ascending way
    * - descending - Results are ordered in descending way
    *
    * @param {String} [options.filterByName=null] Returns only the hierarchies having the given name
    * @param {Boolean} [options.filterByContent=false] Returns only the hierarchies having containing text (the last level partitions in hierarchies)
    * @return {Object} an {@link akomando.hier.identifier} object containing all the identifiers of all the partitions
    * @todo Change the name to getHiers
   */
   getHierIdentifiers({
      sortBy,
      order,
      filterByName,
      filterByContent,
   }={}){
      return this._akn.getHierIdentifiers({
         sortBy,
         order,
         filterByName,
         filterByContent,
      });
   }

   /**
    * Returns the number of elements in the document as a {@link akomando.elements.count} Object
    * @protected
    * @throws {Error} Throws error when the AkomaNtoso document is not set
    * @example <caption>NODE (ES6)</caption>
    * import { createAkomando } from 'akomando';
    *
    * const myAkomaNtosoStream = fs.readFileSync('./my-akomantoso.xml');
    * const myAkomaNtosoString = myAkomaNtosoStream.toString();
    * const myAkomando = createAkomando({
    *    aknString: myAkomaNtosoString,
    * });
    * // Logs the number of all elements contained in the current AkomaNtoso document
    * console.log(myAkomando.countDocElements());
    *
    * // Logs the number of all 'ref' elements contained in the current AkomaNtoso document
    * console.log(myAkomando.countDocElements({
    *    filterByName: 'ref',
    * }));
    * @example <caption>BROWSER</caption>
    * <html>
    *    <head>
    *       <title>akomando example getDocType</title>
    *    </head>
    *    <body>
    *       <script src="https://code.jquery.com/jquery-3.1.1.min.js"
    *               integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
    *               crossorigin="anonymous"></script>
    *       <script src="akomando.min.js" charset="UTF-8"></script>
    *       <script>
    *          // load the content of an AkomaNtoso file in some way. Here I use jQuery.
    *          var myAkomaNtosoString = $.ajax({
    *             url: 'my-akomantoso.xml',
    *             async: false
    *          }).responseText;

    *          var myAkomando =  akomando.createAkomando({
    *             aknString: myAkomaNtosoString,
    *          });
    *
    *          // Logs the number of all elements contained in the current AkomaNtoso document
    *          console.log(myAkomando.countDocElements());
    *
    *          // Logs the number of all 'ref' elements contained in the current AkomaNtoso document
    *          console.log(myAkomando.countDocElements({
    *             filterByName: 'ref',
    *          }));
    *       </script>
    *    </body>
    * </html>
    * @example <caption>COMMAND LINE (linux)</caption>
    * // returns the the number of all elements contained in the current AkomaNtoso document
    * $ akomando count-doc-elements <path/to/file>
    *
    * // returns the number of all 'ref' elements contained in the current AkomaNtoso document
    * $ akomando count-doc-elements <path/to/file> --filter-by-name ref
    * @example <caption>COMMAND LINE (MS-DOS)</caption>
    * // returns the the number of all elements contained in the current AkomaNtoso document
    * $ akomando count-doc-elements "path\to\file"
    *
    * // returns the number of all 'ref' elements contained in the current AkomaNtoso document
    * $ akomando count-doc-elements "path\to\file" --filter-by-name ref
    * @param {Object} [options] The options for restricting the number of elements that must be counted
    * @param {String} [options.filterByName=null] Counts only the elements having the given name
    * @return {Object} The number of elements in the AkomaNtoso documents as a {@link akomando.elements.count} Object
   */
   countDocElements({
      filterByName,
   }={}){
      return this._akn.countDocElements({
         filterByName,
      });
   }

   /**
    * Returns a list of the references that are contained in the metadata section of the document.
    * It also returns information about the references and information about the elements
    * of the document that are linked to them.
    * Results is a JSON structured as a {@link akomando.references.getReferencesInfo}
    * @protected
    * @throws {Error} Throws error when the AkomaNtoso document is not set
    * @throws {Error} Throws error when filterBy parameter is not a String
    * @throws {Error} Throws error when the requested filtering is not supported
    * @param {Object} [options] The options for resulting {@link akomando.references.getReferencesInfo} object
    * @param {String} [options.filterBy=all] Specifies how results must be filtered. Possible values are:
    *
    * - all - returns all the references
    * - used - returns only the references that are used by at least an element in the document
    * - unused - returns only the references that are not used by any element in the document
    *
    * @example <caption>NODE (ES6)</caption>
    * import { createAkomando } from 'akomando';
    *
    * const myAkomaNtosoStream = fs.readFileSync('./my-akomantoso.xml');
    * const myAkomaNtosoString = myAkomaNtosoStream.toString();
    * const myAkomando = createAkomando({
    *    aknString: myAkomaNtosoString,
    * });
    *
    * // Logs the information about all the references in the document
    * console.log(myAkomando.findReferences());
    *
    * // Logs the information about the references that are used at
    * // least by one element in the document
    * console.log(myAkomando.findReferences({
    *   filterBy: 'used'
    * }));
    *
    * // Logs the the information about the references that are not used
    * // by any in the document
    * console.log(myAkomando.findReferences({
    *   filterBy: 'unused'
    * }));
    * @example <caption>BROWSER</caption>
    * <html>
    *    <head>
    *       <title>akomando example getDocType</title>
    *    </head>
    *    <body>
    *       <script src="https://code.jquery.com/jquery-3.1.1.min.js"
    *               integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
    *               crossorigin="anonymous"></script>
    *       <script src="akomando.min.js" charset="UTF-8"></script>
    *       <script>
    *          // load the content of an AkomaNtoso file in some way. Here I use jQuery.
    *          var myAkomaNtosoString = $.ajax({
    *             url: 'my-akomantoso.xml',
    *             async: false
    *          }).responseText;
    *          // creates the akomando object
    *          var myAkomando =  akomando.createAkomando({
    *             aknString: myAkomaNtosoString,
    *          });
    *
    *          // Logs the information about all the references in the document
    *          console.log(myAkomando.findReferences());
    *
    *          // Logs the information about the references that are used at
    *          // least by one element in the document
    *          console.log(myAkomando.findReferences({
    *            filterBy: 'used'
    *          }));
    *
    *          // Logs the information about the references that are not used
    *          // by any element in the document
    *          console.log(myAkomando.findReferences({
    *            filterBy: 'unused'
    *          }));
    *       </script>
    *    </body>
    * </html>
    * @example <caption>COMMAND LINE (linux)</caption>
    * // Returns the information about all the references in the document
    * $ akomando find-references <path/to/file>
    *
    * // Returns the information about the references that are used at
    * // least by one element in the document
    * $ akomando find-references <path/to/file> --filter-by used
    *
    * // Returns the information about the references that are not used
    * // by any element in the document
    * $ akomando find-references <path/to/file> --filter-by unused
    *
    * @example <caption>COMMAND LINE (MS-DOS)</caption>
    * // Returns the information about all the references in the document
    * $ akomando find-references path\to\file
    *
    * // Returns the information about the references that are used at
    * // least by one element in the document
    * $ akomando find-references path\to\file --filter-by used
    *
    * // Returns the information about the references that are not used
    * // by any element in the document
    * $ akomando find-references path\to\file --filter-by unused
    *
    * @return {Object} The list of elements that have a "source" attribute as
    * a {@link akomando.references.getReferencesInfo} Object
   */
   getReferencesInfo({
      filterBy,
   }={}){
      return this._akn.getReferencesInfo({
         filterBy,
      });
   }

  /**
   * Object contained in the list returned by getIdReferences
   * @typedef {Object} akomando.references.idReferences
   * @property {String} referTo The value of the 'refersTo' attribute
   * @property {String} referenceLinkedInAttribute The name of the referring attribute
   * @property {Object} nodeInfo Information of the referencing node
   */

  /**
   * Returns a list of elements that refers to some Id
   * @param {Object} [options] The options for the function
   * @param {Object} [options.filterBy=all] Specifies how results must be filtered. Possible values are:
   * - all - returns all the references
   * - existing - returns only the references to an existing id (in the same document)
   * - pending - returns only the references to an non-exisitng id (in the same document)
   * @return {Object} A list of references, of type {@link akomando.references.idReference}
   */
  getIdReferences({ filterBy='all' }={})
  {
    return this._akn.getIdReferences({ filterBy: filterBy });
  }

  /**
   * Search elements in the document using the given (simplified version of) XPath.
   *
   * Can handle absolute complete path, optionally with indexes
   * Cannot handle relative path (./), deep search (//), attributes, wildcards, predicates or axes.
   *
   * @example
   * // Working examples
   * getElementsFromXPath('/akomaNtoso/act/body/article');
   * getElementsFromXPath('/akomaNtoso/act/meta/classification/keyword[2]');
   *
   * @example
   * // Not working examples
   * getElementsFromXPath('./article');
   * getElementsFromXPath('//article');
   * getElementsFromXPath('/akomaNtoso/act/body/article[@art="art_1"]');
   * getElementsFromXPath('/akomaNtoso/act/meta/*');
   * getElementsFromXPath('/akomaNtoso/act/descendant::meta/');
   * getElementsFromXPath('/akomaNtoso/act/body/article[last()]');
   *
   * @throws {Error} Throws error when xpath parameter is not a String
   * @throws {Error} Throws error when the given string does not match the correct form of the simplified XPath
   * @param {String} xpath The string representing the XPath
   * @return {Object} A list containing all elements represented by the XPath
   */
  getElementsFromXPath(xpath){
    return this._akn.getElementsFromXPath(xpath);
  }

  /**
   * The structure returned by getFRBR
   * @typedef {Object} akomando.FRBR
   * @property {String} work
   * @property {String} expression
   * @property {String} manifestation
   */

  /**
    * Returns an object containing the URIs of Work, Expression and Manifestation of the document
    * @return {Object} An {@link akomando.FRBR} object
    */
  getFRBR() {
    return this._akn.getFRBR();
  }

  /**
   * Returns author and publication/creation date information about Manifestation or Expression or Work of the document
   *
   * @throws {Error} Throws error when the AkomaNtoso document is not set
   * @throws {Error} Throws error when about parameter is not a String
   * @throws {Error} Throws error when the requested filtering is not supported
   * @param {Object} [options] The options for resulting object
   * @param {String} [options.about = expression] Specifies what results we can receive. Possible values are:
   *
   * - work - returns work information
   * - expression - returns expression information
   * - manifestation - returns manifestation information
   *
   * @return {Object} An object with two fields: author, date
   */
  getPublicationInfo({
    about,
  }={}){
    return this._akn.getPublicationInfo({
      about,
    });
  }

  /**
   * Returns the elements with the given id, using a pre-computed map.
   * @param {String} idName The name of the attribute that contains the id (default is 'eId')
   * @param {String} idValue The value of the attribute eId to search
   * @return {Object} A list of XML element with the given Id, if they exist, else undefined
   */
  getElementById({ idName='eId', idValue}={}) {
    return this._akn._IdHandler.getElementById(idName, idValue);
  }

  /**
   * Check whether the ids in the document are well-formed
   * @throws {Error} Throws error when the 'idName' parameter is not a String
   * @throws {Error} Throws error when the 'idName' parameter is not one of: 'all', 'wId', 'eId'
   * @param {Object} options The options for the function call
   * @param {String} options.idName The ids to be checked. Possible values are: 'all' (default), 'wId', 'eId'
   * @return {Object} A list of {@link akomando.document.idError} representing bad ID values
   */
  checkIds({ idName='all' }={}) {
    return this._akn.checkIds({ idName: idName });
  }
}

/**
 * This is the access point of the API. It creates and Akomando object and returns it
 * @param {Object} options Options for building the akomando
 * @param {String} options.aknString The AkomaNtoso string
 * @param {Object} [options.config] The configuration of the akomando according to {@link akomando.config} specications.
 * If config is not send then the ones in config/config.json are used.
 * @return {Object} An {@link _Akomando} object
*/
export function createAkomando({
      aknString,
      config,
   }={}){
   if(typeCheck('Undefined',aknString)){
      throw new Error(errors._AKNStringNotSpecified);
   }
   if(!typeCheck('String',aknString)){
      throw new Error(errors._InputIsNotAString);
   }
   return new Akomando({
      aknString,
      config,
   });
}
