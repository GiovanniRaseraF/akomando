// libs
import _ from 'underscore';

// utils
import { _getElementsAttributes,
         _getElementsByTagNameUniversal,
         _getTextIdentifierObject } from '../constants/utils/utils.js';

/**
 * A class to handle all features related to top level references.
 * For instance, The class can be used for the followings:
 *
 * - to check if all the references in listed in the metadata section are referenced somewhere in the document
 * - to check if elements in documents are linked to not existant references
*/
export default class _ReferencesHandler {

   /**
      * Create an references handler.
   */
   constructor(){

   }

   /**
     * Object schema for the akomando getReferencesInfo command
     * @typedef {Object} akomando.references.getReferencesInfo
     * @property {Object} references The main object
     * @property {Number} references.number The number of references contained in the document
     * @property {String} references.list The list of the references contained in the document
     * @property {String} references.list.name The name of the reference
     * @property {String} references.list.text The text of the reference
     * @property {String} references.list.xpath The xpath of the reference
     * @property {String} references.list.eId The eId attribute of the reference
     * @property {String} references.list.wId The wId attribute of the reference
     * @property {String} references.list.GUID The GUID attribute of the reference
     * @property {String} references.list.href The href attribute of the reference
     * @property {String} references.list.showAs The showAs attribute of the reference
     * @property {Array} references.list.usedBy The list of the elements in the document that are linked to the reference
     * @property {String} references.list.usedBy.nodeName The name of the element
     * @property {String} references.list.usedBy.referenceLinkedInAttribute The name of the attribute in which the reference is linked
     * @property {Object} references.list.usedBy.nodeInfo The information about the element
     * @property {String} references.list.usedBy.nodeInfo.name The name of the reference
     * @property {String} references.list.usedBy.nodeInfo.text The text of the reference
     * @property {String} references.list.usedBy.nodeInfo.xpath The xpath of the reference
     * @property {String} references.list.usedBy.nodeInfo.eId The eId attribute of the reference
     * @property {String} references.list.usedBy.nodeInfo.wId The wId attribute of the reference
     * @property {String} references.list.usedBy.nodeInfo.GUID The GUID attribute of the reference
     * @property {Array} references.list.usedBy.nodeAttributesList The list of the attributes of the element
     * @property {String} references.list.usedBy.attributesList.name The name of the attribute
     * @property {String} references.list.usedBy.attributesList.value The value of the attribute
     *
     * @example
     * {
     *   "references": {
     *     "number": 91,
     *     "list": [
     *       {
     *         "name": "original",
     *         "text": "",
     *         "xpath": "/akomaNtoso[1]/act[1]/meta[1]/references[1]/original[1]",
     *         "eId": "ro1",
     *         "wId": "",
     *         "GUID": "",
     *         "href": "/akn/it/act/decretolegislativo/stato/2016-01-15/8",
     *         "showAs": "",
     *         "usedBy": [
     *           {
     *             "nodeName": "eventRef",
     *             "referenceLinkedInAttribute": "source",
     *             "nodeInfo": {
     *               "name": "eventRef",
     *               "text": "",
     *               "xpath": "/akomaNtoso[1]/act[1]/meta[1]/lifecycle[1]/eventRef[1]",
     *               "eId": "e1",
     *               "wId": "",
     *               "GUID": ""
     *             }
     *             "attributesList": [
     *               {
     *                 "name": "source",
     *                 "value": "#ro1"
     *               },
     *               {
     *                 "name": "type",
     *                 "value": "generation"
     *               },
     *               {
     *                 "name": "eId",
     *                 "value": "e1"
     *               },
     *               {
     *                 "name": "date",
     *                 "value": "2016-02-06"
     *               }
     *             ]
     *           }
     *         ]
     *       }
     *     ]
     *   }
     * }
     *
   */
   /**
    * Returns a list of the references that are contained in the metadata section of the document.
    * It also returns information about the references and information about the elements
    * of the document that are linked to them.
    * Results is a JSON structured as a {@link akomando.references.getReferencesInfo}
    * @param {Object} [options] The options for the resulting {@link akomando.references.getReferencesInfo} object
    * @param {String} [options.namespace=null] - The namespace in which referecences must be searched
    * @param {DOM} [options.AKNDom] - The XML <a target="_blank" href="https://www.w3.org/TR/2000/REC-DOM-Level-2-Core-20001113/core.html#i-Document">
    * @param {Object} [options.config] The configuration of the akomando according to {@link akomando.config} specications.
    * @param {String} [options.filterBy=all] Specifies how results must be filtered. Possible values are:
    * - all - returns all the references
    * - used - returns only the references that are used by at least an element in the document
    * - unused - returns only the references that are not used by any element in the document
    * @return {Object} The list of elements that have a "source" attribute as
    * a {@link akomando.references.getReferencesInfo} Object
   */
   static getReferencesInfo({
      namespace = null,
      AKNDom,
      config,
      filterBy,
   } = {}){
      const referencesName = config.tlcReferences.name;
      const references = _getElementsByTagNameUniversal(
         AKNDom.documentElement.firstChild,
         referencesName,
         namespace
      );

      const referencesList = {
         references : {
            number: 0,
            list: [],
         }
      };

      if(!references[0] || references[0].childNodes.length === 0){
         return referencesList;
      }
      referencesList.references.number = references[0].childNodes.length;
      const elementsAttributesList = _getElementsAttributes(AKNDom);

      for (var i = 0; i < references[0].childNodes.length; i++) {
         const chNode = references[0].childNodes[i];
         if (chNode.nodeType !== references.PROCESSING_INSTRUCTION_NODE &&
               chNode.nodeType !== references.COMMENT_NODE &&
               chNode.nodeType !== references.DOCUMENT_NODE &&
               chNode.nodeType !== references.TEXT_NODE) {
            let reference = _getTextIdentifierObject(chNode);
            let usedByList = _.filter(elementsAttributesList, (obj) => {
               return _.where(obj.attributesList, {
                  value:  `#${chNode.getAttribute('eId')}`,
               }).length > 0;
            });
            usedByList = _.map(usedByList, el => {
               let attribute = _.findWhere(el.attributesList, {
                  value:  `#${chNode.getAttribute('eId')}`,
               }).name;
               return {
                  nodeName: el.nodeName,
                  referenceLinkedInAttribute: attribute,
                  nodeInfo: el.nodeInfo,
                  nodeAttributesList: el.attributesList,
               };
            });
            Object.assign(reference, {
               name: chNode.nodeName,
               href: chNode.getAttribute('href'),
               showAs: chNode.getAttribute('showAs'),
               usedBy: usedByList,
            });
            if(filterBy === 'all'){
               referencesList.references.list.push(reference);
            }
            else if(filterBy === 'used' && reference.usedBy.length > 0){
               referencesList.references.list.push(reference);
            }
            else if(filterBy === 'unused' && reference.usedBy.length === 0){
               referencesList.references.list.push(reference);
            }
         }
      }
      return referencesList;
   }

  /**
   * Returns a list of elements that refers to some Id
   * @param {Object} [options] The options for the function
   * @param {Object} [options.AKNDom] The root of the document DOM
   * @param {Object} [idHandler] An instance of the class _IdHandler
   * @param {Object} [options.filterBy=all] Specifies how results must be filtered. Possible values are:
   * - all - returns all the references
   * - existing - returns only the references to an existing id (in the same document)
   * - pending - returns only the references to an non-exisitng id (in the same document)
   * @return {Object} A list of references, of type {@link akomando.references.idReference}
   */
  static getIdReferences({ AKNDom, idHandler, filterBy='all' } = {})
  {
      const elementsAttributesList = _getElementsAttributes(AKNDom);      // all attributes for each node

      const regexID = /^#\w+$/;

      const allRefers = _.map(elementsAttributesList, (elem) => {
          // referring attributes for each node (could be more than one)
          const referredAttr = _.filter(elem.attributesList, (attr) => (regexID.test(attr.value)));
          return _.map(referredAttr, (attr) => {
              return {
                  refersTo: attr.value.substr(1),  // remove the starting character '#'
                  referenceLinkedInAttribute: attr.name,
                  nodeInfo: elem.nodeInfo
              }
          });
      });

      const allRefersFlatten = _.flatten(allRefers);

      if(filterBy == 'all')
          return allRefersFlatten;
      else if(filterBy == 'existing') // only nodes that refer to an existing Id
          return _.filter(allRefersFlatten, (elem) =>
              (idHandler.getElementById('eId', elem.refersTo) != undefined)
          );
      else if(filterBy == 'pending')  // only nodes that refer to a non-existing Id
          return _.filter(allRefersFlatten, (elem) =>
              (idHandler.getElementById('eId', elem.refersTo) == undefined)
          );
      else
          throw new Error(`Invalid parameter filterBy - ${filterBy}: possible values are 'all', 'existing' and 'pending'`)
  }
}
